﻿using DocumentCreatorProject.Models;
using Microsoft.AspNetCore.Mvc;

namespace DocumentCreatorProject.Controllers
{
    public class HomeController : Controller
    {
        public MptDocCreateContext _context { get; set; }
        MptDocCreateContext db;
        public HomeController(MptDocCreateContext context)
        {
            db = context;
        }


        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Index_Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index_Register(string Name, string Fam, string Ot, DateTime Birth, string Gender, DateTime DateStart, DateTime DateStartMPT, DateTime DateStartTeach, string Email, string Password)
        {
            User user = new User
    {  
            UsersEmail = "Open",
            UsersPassword = "Open",
            UsersStatus = "Open"
        };
            db.Users.Add(user);
            await db.SaveChangesAsync();

            return RedirectToAction("Index");
        }

    }
}
