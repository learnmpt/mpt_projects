﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject.Models;

public partial class User
{

    public int IdUsers { get; set; }

    public string UsersPassword { get; set; }

    public string UsersEmail { get; set; }

    public string UsersStatus { get; set; }

    public virtual ICollection<DocumentsAll> DocumentsAlls { get; } = new List<DocumentsAll>();

    public virtual UsersInfo UsersInfo { get; set; }
}
