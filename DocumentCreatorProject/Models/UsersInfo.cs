﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject.Models;

public partial class UsersInfo
{
    public string UsersInfoFio { get; set; }

    public string UsersInfoLastName { get; set; }

    public string UsersInfoName { get; set; }

    public string UsersInfoMiddleName { get; set; }

    public DateTime UsersInfoDateBirthday { get; set; }

    public DateTime UsersInfoDateStartWork { get; set; }

    public DateTime UsersInfoDateStartWorkMpt { get; set; }

    public DateTime UsersInfoDateStartWorkTeacher { get; set; }

    public int UsersSId { get; set; }

    public int RolesId { get; set; }

    public virtual Role Roles { get; set; }

    public virtual User UsersS { get; set; }
}
