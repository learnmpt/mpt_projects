﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject.Models;

public partial class DocumentsAll
{
    public int IdDocumentsAll { get; set; }

    public string DocumentsAllName { get; set; }

    public int UsersDocId { get; set; }

    public virtual User UsersDoc { get; set; }
}
