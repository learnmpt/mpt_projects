﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject.Models;

public partial class Role
{
    public int IdRoles { get; set; }

    public string RolesName { get; set; }

    public virtual ICollection<UsersInfo> UsersInfos { get; } = new List<UsersInfo>();
}
