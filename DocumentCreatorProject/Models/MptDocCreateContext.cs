﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DocumentCreatorProject.Models;

public partial class MptDocCreateContext : DbContext
{
    public MptDocCreateContext()
    {
    }

    public MptDocCreateContext(DbContextOptions<MptDocCreateContext> options)
        : base(options)
    {
    }

    public virtual DbSet<DocumentsAll> DocumentsAlls { get; set; }

    public virtual DbSet<Role> Roles { get; set; }

    public virtual DbSet<User> Users { get; set; } = null!;

    public virtual DbSet<UsersInfo> UsersInfos { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=LAPTOP-C5VQ52MM\\SQLEXPRESS01;Initial Catalog=MPT_DocCreate;User ID=obraz;Trusted_Connection=True;Encrypt=False;TrustServerCertificate=True;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<DocumentsAll>(entity =>
        {
            entity.HasKey(e => e.IdDocumentsAll).HasName("ID_DocumentsAll");

            entity.ToTable("DocumentsAll");

            entity.Property(e => e.IdDocumentsAll).HasColumnName("ID_DocumentsAll");
            entity.Property(e => e.DocumentsAllName)
                .IsRequired()
                .IsUnicode(false)
                .HasColumnName("DocumentsAll_Name");
            entity.Property(e => e.UsersDocId).HasColumnName("Users_Doc_ID");

            entity.HasOne(d => d.UsersDoc).WithMany(p => p.DocumentsAlls)
                .HasForeignKey(d => d.UsersDocId)
                .HasConstraintName("Users_Doc_ID");
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.HasKey(e => e.IdRoles).HasName("ID_Roles");

            entity.HasIndex(e => e.RolesName, "UQ__Roles__41D67D064CD056EC").IsUnique();

            entity.Property(e => e.IdRoles).HasColumnName("ID_Roles");
            entity.Property(e => e.RolesName)
                .IsRequired()
                .HasMaxLength(20)
                .HasColumnName("Roles_Name");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.IdUsers).HasName("ID_Users");

            entity.HasIndex(e => e.UsersEmail, "UQ__Users__7F0D8B4264C6274F").IsUnique();

            entity.Property(e => e.IdUsers).HasColumnName("ID_Users");
            entity.Property(e => e.UsersEmail)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnName("Users_Email");
            entity.Property(e => e.UsersPassword)
                .IsRequired()
                .HasColumnName("Users_Password");
            entity.Property(e => e.UsersStatus)
                .IsRequired()
                .HasMaxLength(20)
                .HasColumnName("Users_Status");
        });

        modelBuilder.Entity<UsersInfo>(entity =>
        {
            entity.HasKey(e => e.UsersSId);

            entity.ToTable("UsersInfo");

            entity.Property(e => e.UsersSId)
                .ValueGeneratedNever()
                .HasColumnName("Users_S_ID");
            entity.Property(e => e.RolesId).HasColumnName("Roles_ID");
            entity.Property(e => e.UsersInfoDateBirthday)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateBirthday");
            entity.Property(e => e.UsersInfoDateStartWork)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWork");
            entity.Property(e => e.UsersInfoDateStartWorkMpt)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWorkMPT");
            entity.Property(e => e.UsersInfoDateStartWorkTeacher)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWorkTeacher");
            entity.Property(e => e.UsersInfoFio)
                .IsRequired()
                .HasColumnName("UsersInfo_FIO");
            entity.Property(e => e.UsersInfoLastName)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Last_Name");
            entity.Property(e => e.UsersInfoMiddleName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Middle_Name");
            entity.Property(e => e.UsersInfoName)
                .IsRequired()
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Name");

            entity.HasOne(d => d.Roles).WithMany(p => p.UsersInfos)
                .HasForeignKey(d => d.RolesId)
                .HasConstraintName("Roles_ID");

            entity.HasOne(d => d.UsersS).WithOne(p => p.UsersInfo)
                .HasForeignKey<UsersInfo>(d => d.UsersSId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UsersInfo_Users");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
