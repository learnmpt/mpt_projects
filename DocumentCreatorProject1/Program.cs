using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentCreatorProject1;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace WebData
{
    /// <summary>
    /// ��������� ������� ���������
    /// </summary>
    public class Program
    {
        /// <summary>
        /// ������ ���������� �� �����
        /// </summary>
        /// <param name="args">���������</param>
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }
        /// <summary>
        /// ������ ���������� �� �����, ��������� ���������� ������� � Startup
        /// </summary>
        /// <param name="args">���������</param>
        /// <returns>������ ����������</returns>
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
