﻿namespace DocumentCreatorProject1.Models
{
    /// <summary>
    /// Модель представления, объединяющая данные пользователя
    /// </summary>
    public partial class UsersAllInfo
    {
        /// <summary>
        /// ФИО пользователя
        /// </summary>
        public string UsersInfoFio { get; set; } = null!;
        /// <summary>
        /// Фамилия
        /// </summary>
        public string UsersInfoLastName { get; set; } = null!;
        /// <summary>
        /// Имя
        /// </summary>
        public string UsersInfoName { get; set; } = null!;
        /// <summary>
        /// Отчество
        /// </summary>
        public string? UsersInfoMiddleName { get; set; }
        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UsersSId { get; set; }
        /// <summary>
        /// ID пользователя
        /// </summary>
        public int IdUsers { get; set; }
        /// <summary>
        /// Тип категории
        /// </summary>
        public string? KategoryType { get; set; }
        /// <summary>
        /// Дата окончания категории
        /// </summary>
        public DateTime? KategoryDate { get; set; }
        /// <summary>
        /// ID категории
        /// </summary>
        public int IdKategory { get; set; }
        /// <summary>
        /// Email пользователя
        /// </summary>
        public string UsersEmail { get; set; } = null!;
    }

}
