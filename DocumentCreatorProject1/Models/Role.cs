﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject1.Models;
/// <summary>
/// Модель отображения данных роли
/// </summary>
public partial class Role
{
    /// <summary>
    /// ID роли
    /// </summary>
    public int IdRoles { get; set; }
    /// <summary>
    /// Название роли
    /// </summary>
    public string RolesName { get; set; } = null!;
    /// <summary>
    /// Коллекция объектов типа UsersInfo
    /// </summary>
    public virtual ICollection<UsersInfo> UsersInfos { get; } = new List<UsersInfo>();
}
