﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DocumentCreatorProject1.Models;

/// <summary>
/// Контекст для работы с базой данных, необходимый для Web-приложения
/// </summary>
public partial class MptDocCreateContext : DbContext
{
    /// <summary>
    /// Создание связки с конфигурацией проекта
    /// </summary>
    private readonly IConfiguration _configuration;
    /// <summary>
    /// Свойство контекста для работы с БД
    /// </summary>
    public MptDocCreateContext()
    {

    }

    /// <summary>
    /// Конструктор класса MptDocCreateContext. Он принимает два параметра: объект options типа MptDocCreateContext и объект configuration типа IConfiguration.
    /// </summary>
    /// <param name="options">Получение доступа для работы с БД</param>
    /// <param name="configuration">Чтение конфигурационных данных проекта</param>
    public MptDocCreateContext(DbContextOptions<MptDocCreateContext> options, IConfiguration configuration)
        : base(options)
    {
        _configuration = configuration;
    }
    /// <summary>
    /// Создание свойства для доступа к представлению базы данных "DocumentUser"
    /// </summary>
    public virtual DbSet<DocumentUser> DocumentUsers { get; set; }
    /// <summary>
    /// Cоздание свойства для доступа к таблице базы данных "DocumentsAll"
    /// </summary>
    public virtual DbSet<DocumentsAll> DocumentsAlls { get; set; }
    /// <summary>
    /// Cоздание свойства для доступа к таблице базы данных "Kategory"
    /// </summary>
    public virtual DbSet<Kategory> Kategories { get; set; }
    /// <summary>
    /// Cоздание свойства для доступа к таблице базы данных "Role"
    /// </summary>
    public virtual DbSet<Role> Roles { get; set; }
    /// <summary>
    /// Cоздание свойства для доступа к таблице базы данных "User"
    /// </summary>
    public virtual DbSet<User> Users { get; set; }
    /// <summary>
    /// Создание свойства для доступа к представлению базы данных "UsersAllInfo"
    /// </summary>
    public virtual DbSet<UsersAllInfo> UsersAllInfos { get; set; }
    /// <summary>
    /// Cоздание свойства для доступа к таблице базы данных "UsersInfo"
    /// </summary>
    public virtual DbSet<UsersInfo> UsersInfos { get; set; }
    /// <summary>
    /// Создание свойства для доступа к представлению базы данных "UsersWithStatus"
    /// </summary>
    public virtual DbSet<UsersWithStatus> UsersWithStatuses { get; set; }
    /// <summary>
    /// Настройка подключения к базе данных SQL Server используя Connection String, который получается из файла конфигурации приложения
    /// </summary>
    /// <param name="optionsBuilder">Конфигурация опций</param>
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseSqlServer(_configuration.GetConnectionString("DatabaseConnection"));
    /// <summary>
    /// Настройка модели данных в EF Core 
    /// </summary>
    /// <param name="modelBuilder">Конфигураця моделей</param>
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<DocumentUser>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("DocumentUsers");

            entity.Property(e => e.DocumentType).HasColumnName("Document_Type");
            entity.Property(e => e.DocumentUnique).HasColumnName("Document_Unique");
            entity.Property(e => e.DocumentsAllName)
                .HasMaxLength(256)
                .HasColumnName("DocumentsAll_Name");
            entity.Property(e => e.IdDocumentsAll).HasColumnName("ID_DocumentsAll");
            entity.Property(e => e.IdUsers).HasColumnName("ID_Users");
            entity.Property(e => e.UsersDocId).HasColumnName("Users_Doc_ID");
            entity.Property(e => e.UsersInfoFio).HasColumnName("UsersInfo_FIO");
            entity.Property(e => e.UsersStatus)
                .HasMaxLength(20)
                .HasColumnName("Users_Status");
        });

        modelBuilder.Entity<DocumentsAll>(entity =>
        {
            entity.HasKey(e => e.IdDocumentsAll).HasName("ID_DocumentsAll");

            entity.ToTable("DocumentsAll");

            entity.HasIndex(e => new { e.DocumentsAllName, e.UsersDocId }, "UnicFile").IsUnique();

            entity.Property(e => e.IdDocumentsAll).HasColumnName("ID_DocumentsAll");
            entity.Property(e => e.DocumentType).HasColumnName("Document_Type");
            entity.Property(e => e.DocumentUnique).HasColumnName("Document_Unique");
            entity.Property(e => e.DocumentsAllName)
                .HasMaxLength(256)
                .HasColumnName("DocumentsAll_Name");
            entity.Property(e => e.UsersDocId).HasColumnName("Users_Doc_ID");

            entity.HasOne(d => d.UsersDoc).WithMany(p => p.DocumentsAlls)
                .HasForeignKey(d => d.UsersDocId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DocumentsAll_Users1");
        });

        modelBuilder.Entity<Kategory>(entity =>
        {
            entity.HasKey(e => e.IdKategory);

            entity.ToTable("Kategory");

            entity.Property(e => e.IdKategory)
                .ValueGeneratedOnAdd()
                .HasColumnName("ID_Kategory");
            entity.Property(e => e.KategoryDate)
                .HasColumnType("datetime")
                .HasColumnName("Kategory_Date");
            entity.Property(e => e.KategoryType)
                .HasMaxLength(50)
                .HasColumnName("Kategory_Type");

            entity.HasOne(d => d.IdKategoryNavigation).WithOne(p => p.Kategory)
                .HasForeignKey<Kategory>(d => d.IdKategory)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Kategory_Users");
        });

        modelBuilder.Entity<Role>(entity =>
        {
            entity.HasKey(e => e.IdRoles).HasName("ID_Roles");

            entity.HasIndex(e => e.RolesName, "UQ__Roles__41D67D0603C4A7E3").IsUnique();

            entity.Property(e => e.IdRoles).HasColumnName("ID_Roles");
            entity.Property(e => e.RolesName)
                .HasMaxLength(20)
                .HasColumnName("Roles_Name");
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.HasKey(e => e.IdUsers).HasName("ID_Users");

            entity.ToTable(tb => tb.HasTrigger("trg_CreateUser"));

            entity.HasIndex(e => e.UsersEmail, "UQ__Users__7F0D8B4264C6274F").IsUnique();

            entity.Property(e => e.IdUsers).HasColumnName("ID_Users");
            entity.Property(e => e.UsersEmail)
                .HasMaxLength(50)
                .HasColumnName("Users_Email");
            entity.Property(e => e.UsersPassword).HasColumnName("Users_Password");
            entity.Property(e => e.UsersStatus)
                .HasMaxLength(20)
                .HasColumnName("Users_Status");
        });

        modelBuilder.Entity<UsersAllInfo>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("UsersAllInfo");

            entity.Property(e => e.IdKategory).HasColumnName("ID_Kategory");
            entity.Property(e => e.IdUsers).HasColumnName("ID_Users");
            entity.Property(e => e.KategoryDate)
                .HasColumnType("datetime")
                .HasColumnName("Kategory_Date");
            entity.Property(e => e.KategoryType)
                .HasMaxLength(50)
                .HasColumnName("Kategory_Type");
            entity.Property(e => e.UsersEmail)
                .HasMaxLength(50)
                .HasColumnName("Users_Email");
            entity.Property(e => e.UsersInfoFio).HasColumnName("UsersInfo_FIO");
            entity.Property(e => e.UsersInfoLastName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Last_Name");
            entity.Property(e => e.UsersInfoMiddleName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Middle_Name");
            entity.Property(e => e.UsersInfoName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Name");
            entity.Property(e => e.UsersSId).HasColumnName("Users_S_ID");
        });

        modelBuilder.Entity<UsersInfo>(entity =>
        {
            entity.HasKey(e => e.UsersSId);

            entity.ToTable("UsersInfo");

            entity.Property(e => e.UsersSId)
                .ValueGeneratedNever()
                .HasColumnName("Users_S_ID");
            entity.Property(e => e.RolesId).HasColumnName("Roles_ID");
            entity.Property(e => e.UsersInfoDateBirthday)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateBirthday");
            entity.Property(e => e.UsersInfoDateStartWork)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWork");
            entity.Property(e => e.UsersInfoDateStartWorkMpt)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWorkMPT");
            entity.Property(e => e.UsersInfoDateStartWorkTeacher)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWorkTeacher");
            entity.Property(e => e.UsersInfoFio).HasColumnName("UsersInfo_FIO");
            entity.Property(e => e.UsersInfoLastName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Last_Name");
            entity.Property(e => e.UsersInfoMiddleName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Middle_Name");
            entity.Property(e => e.UsersInfoName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Name");

            entity.HasOne(d => d.Roles).WithMany(p => p.UsersInfos)
                .HasForeignKey(d => d.RolesId)
                .HasConstraintName("Roles_ID");

            entity.HasOne(d => d.UsersS).WithOne(p => p.UsersInfo)
                .HasForeignKey<UsersInfo>(d => d.UsersSId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_UsersInfo_Users");
        });

        modelBuilder.Entity<UsersWithStatus>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("UsersWithStatus");

            entity.Property(e => e.RolesId).HasColumnName("Roles_ID");
            entity.Property(e => e.UsersInfoDateBirthday)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateBirthday");
            entity.Property(e => e.UsersInfoDateStartWork)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWork");
            entity.Property(e => e.UsersInfoDateStartWorkMpt)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWorkMPT");
            entity.Property(e => e.UsersInfoDateStartWorkTeacher)
                .HasColumnType("datetime")
                .HasColumnName("UsersInfo_DateStartWorkTeacher");
            entity.Property(e => e.UsersInfoFio).HasColumnName("UsersInfo_FIO");
            entity.Property(e => e.UsersInfoLastName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Last_Name");
            entity.Property(e => e.UsersInfoMiddleName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Middle_Name");
            entity.Property(e => e.UsersInfoName)
                .HasMaxLength(50)
                .HasColumnName("UsersInfo_Name");
            entity.Property(e => e.UsersSId).HasColumnName("Users_S_ID");
            entity.Property(e => e.UsersStatus)
                .HasMaxLength(20)
                .HasColumnName("Users_Status");
        });

        OnModelCreatingPartial(modelBuilder);
    }
    /// <summary>
    /// Определение настроек базы данных (добавление дополнительных настроек)
    /// </summary>
    /// <param name="modelBuilder">Конфигурация модели</param>
    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}

