﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject1.Models;
/// <summary>
/// Модель представления Категории пользователя
/// </summary>
public partial class Kategory
{
    /// <summary>
    /// Параметр типа категории
    /// </summary>
    public string? KategoryType { get; set; }
    /// <summary>
    /// Параметр даты окончания категории
    /// </summary>
    public DateTime? KategoryDate { get; set; }
    /// <summary>
    /// Параметр ID категории
    /// </summary>
    public int IdKategory { get; set; }
    /// <summary>
    /// Наследование модели User
    /// </summary>
    public virtual User IdKategoryNavigation { get; set; } = null!;
}
