﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject1.Models;
/// <summary>
/// Модель представления данных пользователя
/// </summary>
public partial class UsersInfo
{
    /// <summary>
    /// ФИО пользователя
    /// </summary>
    public string UsersInfoFio { get; set; } = null!;
    /// <summary>
    /// Фамилия
    /// </summary>
    public string UsersInfoLastName { get; set; } = null!;
    /// <summary>
    /// Имя
    /// </summary>
    public string UsersInfoName { get; set; } = null!;
    /// <summary>
    /// Отчество
    /// </summary>
    public string? UsersInfoMiddleName { get; set; }
    /// <summary>
    /// Дата рождения
    /// </summary>
    public DateTime UsersInfoDateBirthday { get; set; }
    /// <summary>
    /// Дата начала работы
    /// </summary>
    public DateTime UsersInfoDateStartWork { get; set; }
    /// <summary>
    /// Дата начала работы в МПТ
    /// </summary>
    public DateTime UsersInfoDateStartWorkMpt { get; set; }
    /// <summary>
    /// Дата начала преподавательской деятельности
    /// </summary>
    public DateTime UsersInfoDateStartWorkTeacher { get; set; }
    /// <summary>
    /// ID пользователя
    /// </summary>
    public int UsersSId { get; set; }
    /// <summary>
    /// ID роли
    /// </summary>
    public int RolesId { get; set; }
    /// <summary>
    /// Наследование модели Role
    /// </summary>
    public virtual Role Roles { get; set; } = null!;
    /// <summary>
    /// Наследование модели User
    /// </summary>
    public virtual User UsersS { get; set; } = null!;
}
