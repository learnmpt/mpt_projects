﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject1.Models;
/// <summary>
/// Модель представления, объединяющая пользователя и его документы
/// </summary>
public partial class DocumentUser
{
    /// <summary>
    /// Параметр ID документа
    /// </summary>
    public int IdDocumentsAll { get; set; }
    /// <summary>
    /// Параметр ID пользователя
    /// </summary>
    public int UsersDocId { get; set; }
    /// <summary>
    /// Параметр полного названия документа
    /// </summary>
    public string DocumentsAllName { get; set; } = null!;
    /// <summary>
    /// Параметр типа документа
    /// </summary>
    public int DocumentType { get; set; }
    /// <summary>
    /// Параметр уникальности документа
    /// </summary>
    public int? DocumentUnique { get; set; }
    /// <summary>
    /// Изменяемый параметр, присущий разным видам документа. Не имеет какого-то определенного значения, так как оно меняется
    /// в зависимости от типа документа
    /// </summary>
    public string? Parametr1 { get; set; }
    /// <summary>
    /// Изменяемый параметр, присущий разным видам документа. Не имеет какого-то определенного значения, так как оно меняется
    /// в зависимости от типа документа
    /// </summary>
    public string? Parametr2 { get; set; }
    /// <summary>
    /// Изменяемый параметр, присущий разным видам документа. Не имеет какого-то определенного значения, так как оно меняется
    /// в зависимости от типа документа
    /// </summary>
    public string? Parametr3 { get; set; }
    /// <summary>
    /// Изменяемый параметр, присущий разным видам документа. Не имеет какого-то определенного значения, так как оно меняется
    /// в зависимости от типа документа
    /// </summary>
    public string? Parametr4 { get; set; }
    /// <summary>
    /// Изменяемый параметр, присущий разным видам документа. Не имеет какого-то определенного значения, так как оно меняется
    /// в зависимости от типа документа
    /// </summary>
    public string? Parametr5 { get; set; }
    /// <summary>
    /// Изменяемый параметр, присущий разным видам документа. Не имеет какого-то определенного значения, так как оно меняется
    /// в зависимости от типа документа
    /// </summary>
    public string? Parametr6 { get; set; }
    /// <summary>
    /// Изменяемый параметр, присущий разным видам документа. Не имеет какого-то определенного значения, так как оно меняется
    /// в зависимости от типа документа
    /// </summary>
    public string? Parametr7 { get; set; }
    /// <summary>
    /// Параметр ID пользователя
    /// </summary>
    public int IdUsers { get; set; }
    /// <summary>
    /// Параметр статус пользователя
    /// </summary>
    public string UsersStatus { get; set; } = null!;
    /// <summary>
    /// Параметр ФИО пользователя
    /// </summary>
    public string UsersInfoFio { get; set; } = null!;
}
