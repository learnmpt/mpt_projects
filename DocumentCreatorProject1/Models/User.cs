﻿using System;
using System.Collections.Generic;

namespace DocumentCreatorProject1.Models;
/// <summary>
/// Модель отображения данных пользователя
/// </summary>
public partial class User
{
    /// <summary>
    /// ID пользователя
    /// </summary>
    public int IdUsers { get; set; }
    /// <summary>
    /// Пароль пользователя
    /// </summary>
    public string UsersPassword { get; set; } = null!;
    /// <summary>
    /// Email пользователя
    /// </summary>
    public string UsersEmail { get; set; } = null!;
    /// <summary>
    /// Статус пользователя
    /// </summary>
    public string UsersStatus { get; set; } = null!;
    /// <summary>
    /// Коллекция объектов типа DocumentsAll
    /// </summary>
    public virtual ICollection<DocumentsAll> DocumentsAlls { get; } = new List<DocumentsAll>();
    /// <summary>
    /// Наследование модели Kategory
    /// </summary>
    public virtual Kategory? Kategory { get; set; }
    /// <summary>
    /// Наследование моделии UsersInfo
    /// </summary>
    public virtual UsersInfo? UsersInfo { get; set; }
}
