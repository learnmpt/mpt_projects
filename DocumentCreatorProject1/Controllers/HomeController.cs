﻿using DocumentCreatorProject1.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using Xceed.Words.NET;
using YandexDisk.Client;
using YandexDisk.Client.Clients;
using YandexDisk.Client.Http;
using YandexDisk.Client.Protocol;
using Newtonsoft.Json.Linq;
using System.Web;


namespace DocumentCreatorProject1.Controllers
{

    /// <summary>
    /// Основной контроллер взаимодействия компонентов
    /// </summary>
    public class HomeController : Controller
    {

        /// <summary>
        /// Контекст взаимодействия с бд
        /// </summary>
        public MptDocCreateContext _context { get; set; }
        /// <summary>
        /// Фамилия пользователя для работы с файлами
        /// </summary>
        public string surname = "";
        /// <summary>
        /// Создание массива для корректного отображения названия месяца
        /// </summary>
        readonly string[] Month = { " ", "января", "февраля", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря" };
        /// <summary>
        /// Создание связки с БД
        /// </summary>
        readonly MptDocCreateContext db;
        /// <summary>
        /// Конструктор класса HomeController. Он принимает два параметра: объект context типа MptDocCreateContext и объект configuration типа IConfiguration.
        /// </summary>
        /// <param name="context">Получение доступа для работы с БД</param>
        /// <param name="configuration">Чтение конфигурационных данных проекта</param>
        public HomeController(MptDocCreateContext context, IConfiguration configuration)
        {
            db = context;
            _configuration = configuration;
        }
        /// <summary>
        /// Создание связки с конфигурацией проекта
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Метод, направленный на обработку данных при загрузке на удаленное облачное хранилище
        /// Яндекс диск
        /// </summary>
        /// <param name="FileUrl">Загруженный файл</param>
        /// <param name="TypeFile">Тип файла</param>
        /// <param name="NewDate">Дата документа</param>
        /// <param name="NewName">Изменяемый параметр</param>
        /// <param name="TypeUser">Тип участия в соревновании</param>
        /// <param name="TypeWSR">Тип WSR или DigitalSkills</param>
        /// <param name="TypeYMK">Тип УМК</param>
        /// <param name="NewName2">Изменяемый параметр</param>
        /// <param name="NewName3">Изменяемый параметр</param>
        /// <param name="NewDate2">Изменяемый параметр даты</param>
        /// <param name="NewName4">Изменяемый параметр</param>
        /// <param name="NewName5">Изменяемый параметр</param>
        /// <param name="NewName6">Изменяемый параметр</param>
        /// <param name="NewName7">Изменяемый параметр</param>
        /// <param name="NewName8">Изменяемый параметр</param>
        /// <param name="users">Массив пользователей, которым необходимо загрузить документ в портфолио</param>
        /// <returns>Сообщение с описанием успешности загрузки</returns>

        [HttpPost]
        [DisableRequestSizeLimit]
        public async Task<string> InsertFileToServer(IFormFile FileUrl, string TypeFile, DateTime NewDate, string NewName, string TypeUser, string TypeWSR, string TypeYMK, string NewName2, string NewName3, DateTime NewDate2, string NewName4, string NewName5, string NewName6, string NewName7, string NewName8, string[] users)
        {
            try
            {
                string extension = (FileUrl.FileName).ToString();
                int ind = extension.LastIndexOf(".");
                string nametranslit = extension.Substring(0, ind);
                extension = extension.Substring(ind + 1);
                TranslitMethods.Translitter trn = new TranslitMethods.Translitter();
                nametranslit = trn.Translit(nametranslit, TranslitMethods.TranslitType.Gost).ToString() + "." + extension;
                if (nametranslit.Length <= 255)
                {
                    int numberString = 0;
                    string errorMessage = "";
                    if (users.Length != 0)
                    {
                        try
                        {
                            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();
                            // Метод транслитерации служит для обработки файлов сервером, так как на нем не поддерживается загрузка файлов
                            // имеющих в названии русский символы

                            using (Stream fileStream = new FileStream(Directory.GetCurrentDirectory() + "/wwwroot/file/" + nametranslit, FileMode.Create, FileAccess.Write))
                            {
                                FileUrl.CopyTo(fileStream);
                            }
                            // Вызов токена из конфигурации проекта. Код доступа необходимо менять раз в год, получение нового токена возможна через расширенный доступ
                            // к аккаунту Яндекс диск. Выдача токена доступа к диску доступно по ссылке https://oauth.yandex.ru/client/new
                            string oauthToken = _configuration.GetConnectionString("TokenAuth")!;
                            var api = new DiskHttpApi(oauthToken);
                            IDiskApi diskApi = new DiskHttpApi(oauthToken);

                            int numberUser = 0;
                            // Начало перебора файла для занесения на Яндекс диск. Алгоритм проходит по каждому из выбранных пользователей
                            foreach (var user in users)
                            {
                                int id = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers;
                                // DiskCheckerAsync позволяет перед записью проверить синхронизацию записей о файлах в базе, и реально находящихся на диске файлов.
                                // Файлы, которые являются лишними, или записи в базе, не имеющие к себе подвязку в виде реального файла на диске - удаляются.
                                await DiskCheckerAsync(id);
                                string Name = "";
                                // Наименование файлов регламентируется шаблонным образцом наименованию файлов каждой из категорий.
                                string User = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().UsersInfoLastName;
                                if (TypeFile == "1")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "НПО. " + NewName + ". " + NewName2; //done
                                }
                                else if (TypeFile == "2")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "СПО. " + NewName + ". " + NewName2 + ". " + NewName4;//done
                                }
                                else if (TypeFile == "3")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "ВПО. " + NewName + ". " + NewName2 + ". " + NewName4;//done
                                }
                                else if (TypeFile == "4")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Вендорная сертификация. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "5")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сертификация. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "6")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сертификат. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "7")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сертификат студента. " + NewName + ". " + NewName2 + ". " + NewName4 + ". " + NewName5;//done
                                }
                                else if (TypeFile == "8")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сертификация студента. " + NewName + ". " + NewName2 + ". " + NewName4;//done
                                }
                                else if (TypeFile == "9")
                                {
                                    if (NewName7 != null)
                                    {
                                        Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сертификат студента. WSR. " + TypeWSR + ". " + NewName5 + ". " + NewName6 + ". " + NewName7;//done
                                    }
                                    else
                                    {
                                        Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сертификат студента. WSR. " + TypeWSR + ". " + NewName5 + ". " + NewName6;//done
                                    }
                                }
                                else if (TypeFile == "10")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Участие в WSR. " + TypeUser + ". " + TypeWSR + ". " + NewName2 + ". " + NewName4;//done
                                }
                                else if (TypeFile == "11")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Участие в WSR. ЧЭ. " + NewName; //done
                                }
                                else if (TypeFile == "12")
                                {
                                    // Так как присвоенная квалификация образования является опциональным параметром, добавление ее в название зависит от заполнения данного пункта

                                    if (NewName4 == null)
                                    {
                                        Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "ПП. " + NewName + ". " + NewName2;//done
                                    }
                                    else
                                    {
                                        Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "ПП. " + NewName + ". " + NewName2 + "(с присвоением квалификации " + NewName4 + ")";
                                    }
                                }
                                else if (TypeFile == "13")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "ПК. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "14")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "ПО. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "15")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Распоряжение. " + NewName + ". " + NewName3 + " от " + NewDate2.ToString("dd.MM.yyyy ") + NewName8;//done
                                }
                                else if (TypeFile == "16")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Приказ. " + NewName + ". " + NewName3 + " от " + NewDate2.ToString("dd.MM.yyyy ") + NewName8;//done
                                }
                                else if (TypeFile == "17")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Конференция. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "18")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Мастер-класс. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "19")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Форум. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "20")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Панельная дискуссия. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "21")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Онлайн-класс. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "22")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Вебинар. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "23")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Семинар. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "24")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Круглый стол. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "25")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Отчёт. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "26")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Справка. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "27")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Справка. Публикация. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "28")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Справка. Стажировка. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "29")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Статья. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "30")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Свидетельство. Публикация. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "31")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сводная ведомость. " + NewName + ". " + NewName2 + ". " + NewName4;//done
                                }
                                else if (TypeFile == "32")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Стажировка. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "33")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "УМК. " + NewName + ". " + TypeYMK + ". " + NewName4 + ". " + NewName5 + ". " + NewName6;//done
                                }
                                else if (TypeFile == "34")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "НИР. " + NewName + ". " + NewName2 + ". " + NewName4;//done
                                }
                                else if (TypeFile == "35")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Благодарность. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "36")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Награда. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "37")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Конкурс педагогов. " + NewName + ". " + NewName2;//done
                                }
                                else if (TypeFile == "38")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Бейджик. " + NewName;//done
                                }
                                else if (TypeFile == "39")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Профессиональные ассоциации. " + NewName;//done
                                }
                                else if (TypeFile == "40")
                                {
                                    if (NewName7 != null)
                                    {
                                        Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сертификат студента. DigitalSkills. " + TypeWSR + ". " + NewName5 + ". " + NewName6 + ". " + NewName7;//done
                                    }
                                    else
                                    {
                                        Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Сертификат студента. DigitalSkills. " + TypeWSR + ". " + NewName5 + ". " + NewName6;//done
                                    }
                                }
                                else if (TypeFile == "41")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Участие в DigitalSkills. " + TypeUser + ". " + TypeWSR + ". " + NewName2 + ". " + NewName4;//done
                                }
                                else if (TypeFile == "42")
                                {
                                    Name = User + ". " + NewDate.ToString("yyyy. MM. dd. ") + "Протокол. Открытый урок. " + NewName + ". " + NewName2; //done
                                }

                                // Замена недопустимых к обработке символов

                                Name = Name.Replace("/", "-");
                                Name = Name.Replace(":", " ");
                                Debug.Write(Name);
                                int i = 1;

                                string Altername = Name;
                                string FolderUser = users[numberUser];
                                if ((Altername.Length + extension.Length + 1) > 255)
                                {
                                    errorMessage = errorMessage + FolderUser + "(Название файла превышает 255 символов); ";
                                    numberUser++;
                                    i = 0;
                                    numberString = 0;
                                }
                                else
                                {
                                    // Добавление записи о файле в базу по параметрам. За уникальность файла отвечает параметр DocumentUnique, в создании документов участвуют только уникальные документы
                                    // Уникальность файла не зависит от расширения. Из двух файлов с одинаковым названием, но разным расширением, только первый загруженный файл будет считаться уникальным

                                    try
                                    {
                                        var usersdoc = await db.DocumentUsers.Where(model => model.UsersInfoFio == users[numberUser] && (model.DocumentsAllName.Contains(Name)) && model.DocumentUnique == 1).ToListAsync();
                                        if (usersdoc.Count == 0)
                                        {
                                            DocumentsAll Doc = new DocumentsAll { };
                                            if (TypeFile == "1" ||
                                                TypeFile == "4" ||
                                                TypeFile == "5" ||
                                                TypeFile == "6" ||
                                                TypeFile == "13" ||
                                                TypeFile == "14" ||
                                                TypeFile == "17" ||
                                                TypeFile == "18" ||
                                                TypeFile == "19" ||
                                                TypeFile == "20" ||
                                                TypeFile == "21" ||
                                                TypeFile == "22" ||
                                                TypeFile == "23" ||
                                                TypeFile == "24" ||
                                                TypeFile == "25" ||
                                                TypeFile == "26" ||
                                                TypeFile == "27" ||
                                                TypeFile == "28" ||
                                                TypeFile == "29" ||
                                                TypeFile == "30" ||
                                                TypeFile == "32" ||
                                                TypeFile == "35" ||
                                                TypeFile == "36" ||
                                                TypeFile == "37" ||
                                                TypeFile == "42")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),

                                                    Parametr2 = NewName,
                                                    Parametr3 = NewName2



                                                };
                                            }
                                            else if (TypeFile == "2" ||
                                                TypeFile == "3" ||
                                                TypeFile == "8" ||
                                                TypeFile == "12" ||
                                                TypeFile == "34")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),

                                                    Parametr2 = NewName,
                                                    Parametr3 = NewName2,
                                                    Parametr4 = NewName4



                                                };
                                            }
                                            else if (TypeFile == "11" ||
                                                TypeFile == "38" ||
                                                TypeFile == "39")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                    Parametr2 = NewName



                                                };

                                            }
                                            else if (TypeFile == "15" || TypeFile == "16")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                    Parametr2 = NewName,
                                                    Parametr3 = NewName3 + " от " + NewDate2.ToString("dd.MM.yyyy ") + NewName8



                                                };
                                            }
                                            else if (TypeFile == "7")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                    Parametr2 = NewName,
                                                    Parametr3 = NewName2,
                                                    Parametr4 = NewName4,
                                                    Parametr5 = NewName5



                                                };
                                            }
                                            else if (TypeFile == "9" || TypeFile == "40")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                    Parametr2 = TypeWSR,
                                                    Parametr3 = NewName5,
                                                    Parametr4 = NewName6,
                                                    Parametr5 = NewName7



                                                };
                                            }
                                            else if (TypeFile == "10" || TypeFile == "41")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                    Parametr2 = TypeUser,
                                                    Parametr3 = TypeWSR,
                                                    Parametr4 = NewName2,
                                                    Parametr5 = NewName4



                                                };
                                            }
                                            else if (TypeFile == "33")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                    Parametr2 = NewName,
                                                    Parametr3 = TypeYMK,
                                                    Parametr4 = NewName4,
                                                    Parametr5 = NewName5,
                                                    Parametr6 = NewName6



                                                };
                                            }
                                            else if (TypeFile == "31")
                                            {
                                                Doc = new DocumentsAll
                                                {

                                                    DocumentsAllName = Name + "." + extension,
                                                    DocumentType = int.Parse(TypeFile),
                                                    UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                    DocumentUnique = 1,
                                                    Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                    Parametr2 = NewName,
                                                    Parametr3 = NewName2.Replace("-", "/"),
                                                    Parametr4 = NewName4,
                                                    Parametr5 = NewName5,
                                                    Parametr6 = NewName6.Replace("%", "")



                                                };
                                            }
                                            db.DocumentsAlls.Add(Doc);

                                            db.SaveChanges();

                                        }
                                        else
                                        {
                                            var usersdoc2 = db.DocumentUsers.Where(model => model.UsersInfoFio == users[numberUser] && model.DocumentUnique == 0).Distinct().ToList();
                                            while (true)
                                            {
                                                Altername = Name + "(" + i + ")";
                                                var unicFile = usersdoc2.Where(model => model.DocumentsAllName.Contains(Altername)).ToList();
                                                if ((Altername.Length + extension.Length + 1) > 255)
                                                {
                                                    errorMessage = errorMessage + FolderUser + "(Название файла превышает 255 символов); ";
                                                    numberString = 1;
                                                    break;
                                                }
                                                else
                                                {
                                                    if (unicFile.Count == 0)
                                                    {
                                                        DocumentsAll Doc = new DocumentsAll { };
                                                        if (TypeFile == "1" ||
                                                            TypeFile == "4" ||
                                                            TypeFile == "5" ||
                                                            TypeFile == "6" ||
                                                            TypeFile == "13" ||
                                                            TypeFile == "14" ||
                                                            TypeFile == "17" ||
                                                            TypeFile == "18" ||
                                                            TypeFile == "19" ||
                                                            TypeFile == "20" ||
                                                            TypeFile == "21" ||
                                                            TypeFile == "22" ||
                                                            TypeFile == "23" ||
                                                            TypeFile == "24" ||
                                                            TypeFile == "25" ||
                                                            TypeFile == "26" ||
                                                            TypeFile == "27" ||
                                                            TypeFile == "28" ||
                                                            TypeFile == "29" ||
                                                            TypeFile == "30" ||
                                                            TypeFile == "32" ||
                                                            TypeFile == "35" ||
                                                            TypeFile == "36" ||
                                                            TypeFile == "37" ||
                                                            TypeFile == "42")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),

                                                                Parametr2 = NewName,
                                                                Parametr3 = NewName2



                                                            };
                                                        }
                                                        else if (TypeFile == "2" ||
                                                            TypeFile == "3" ||
                                                            TypeFile == "8" ||
                                                            TypeFile == "12" ||
                                                            TypeFile == "34")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),

                                                                Parametr2 = NewName,
                                                                Parametr3 = NewName2,
                                                                Parametr4 = NewName4



                                                            };
                                                        }
                                                        else if (TypeFile == "11" ||
                                                            TypeFile == "38" ||
                                                            TypeFile == "39")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                                Parametr2 = NewName



                                                            };

                                                        }
                                                        else if (TypeFile == "15" || TypeFile == "16")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                                Parametr2 = NewName,
                                                                Parametr3 = NewName3 + " от " + NewDate2.ToString("dd.MM.yyyy ") + NewName8



                                                            };
                                                        }
                                                        else if (TypeFile == "7")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                                Parametr2 = NewName,
                                                                Parametr3 = NewName2,
                                                                Parametr4 = NewName3,
                                                                Parametr5 = NewName4,
                                                                Parametr6 = NewName5,
                                                                Parametr7 = NewName6



                                                            };
                                                        }
                                                        else if (TypeFile == "9" || TypeFile == "40")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                                Parametr2 = TypeWSR,
                                                                Parametr3 = NewName5,
                                                                Parametr4 = NewName6,
                                                                Parametr5 = NewName7



                                                            };
                                                        }
                                                        else if (TypeFile == "10" || TypeFile == "41")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                                Parametr2 = TypeUser,
                                                                Parametr3 = TypeWSR,
                                                                Parametr4 = NewName2,
                                                                Parametr5 = NewName4



                                                            };
                                                        }
                                                        else if (TypeFile == "33")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                                Parametr2 = NewName,
                                                                Parametr3 = TypeYMK,
                                                                Parametr4 = NewName4,
                                                                Parametr5 = NewName5,
                                                                Parametr6 = NewName6



                                                            };
                                                        }
                                                        else if (TypeFile == "31")
                                                        {
                                                            Doc = new DocumentsAll
                                                            {

                                                                DocumentsAllName = Altername + "." + extension,
                                                                DocumentType = int.Parse(TypeFile),
                                                                UsersDocId = db.UsersAllInfos.Where(a => a.UsersInfoFio == users[numberUser]).Single().IdUsers,
                                                                DocumentUnique = 0,
                                                                Parametr1 = NewDate.ToString("dd.MM.yyyy "),
                                                                Parametr2 = NewName,
                                                                Parametr3 = NewName2.Replace("-", "/"),
                                                                Parametr4 = NewName4,
                                                                Parametr5 = NewName5,
                                                                Parametr6 = NewName6.Replace("%", "")



                                                            };
                                                        }

                                                        db.DocumentsAlls.Add(Doc);
                                                        db.SaveChanges();
                                                        break;
                                                    }

                                                    i++;
                                                }

                                            }
                                        }

                                    }
                                    catch
                                    {

                                    }


                                    if (numberString == 0)
                                    {

                                        try
                                        {
                                            //В path составляется полный путь до папки пользователя, которому необходимо добавление файла. Так как Яндекс диск
                                            //имеет ограничение на загрузку, наиболее быстрым способом является загрузка файлов в формате txt с дальнейшей конвертацией типа в изначальный.
                                            //Доказательства различия времени загрузки файлов представлены иными пользователями, столкнувшимися с такой же проблемой.
                                            //Загрузка файла 11.txt завершена, затрачено времени: 0:00:41.881488
                                            //Загрузка файла 11.jpg завершена, затрачено времени: 0:00:41.826355
                                            //Загрузка файла 11.png завершена, затрачено времени: 0:00:51.993082
                                            //Загрузка файла 11.rar завершена, затрачено времени: 0:34:46.165469
                                            //Загрузка файла 11.zip завершена, затрачено времени: 0:34:50.776791
                                            //Загрузка файла 11 завершена, затрачено времени: 0:00:56.583622
                                            //Загрузка файла 11.html завершена, затрачено времени: 0:00:45.933040
                                            //Загрузка файла 11.bat завершена, затрачено времени: 0:00:47.237989
                                            //Загрузка файла 11.rtf завершена, затрачено времени: 0:00:56.147034
                                            //Загрузка файла 11.dat завершена, затрачено времени: 0:34:50.133080
                                            //Загрузка файла 11.exe завершена, затрачено времени: 0:04:17.051915
                                            //Загрузка файла 11.gif завершена, затрачено времени: 0:00:54.799939
                                            //Загрузка файла 11.mp3 завершена, затрачено времени: 0:00:56.603038
                                            //Загрузка файла 11.mp4 завершена, затрачено времени: 0:34:50.421615
                                            db.SaveChanges();
                                            await diskApi.Files.UploadFileAsync(path: "портфолио преподавателей/" + FolderUser + "/" + Altername + ".txt",

                                                                                overwrite: false,
                                                                                localFile: Directory.GetCurrentDirectory() + "/wwwroot/file/" + nametranslit,

                                                                                cancellationToken: CancellationToken.None);

                                            var request = new MoveFileRequest
                                            {
                                                From = "портфолио преподавателей/" + FolderUser + "/" + Altername + ".txt",
                                                Path = "портфолио преподавателей/" + FolderUser + "/" + Altername + "." + extension,
                                                Overwrite = false
                                            };
                                            await diskApi.Commands.MoveAndWaitAsync(
                                                request: request,
                                                cancellationToken: CancellationToken.None,
                                                pullPeriod: null
                                               );


                                        }
                                        catch
                                        {
                                            errorMessage = errorMessage + FolderUser + "(Ошибка загрузки файла в папку); ";
                                        }
                                        numberUser++;
                                        i = 0;

                                    }
                                    else
                                    {
                                        numberUser++;
                                        i = 0;
                                        numberString = 0;
                                    }
                                }
                            }
                            //Так как система загружает файл на сервер перед загрузкой на диск, после его обработки файл необходимо удалить. Directory.GetCurrentDirectory() позволяет
                            //получить текущую директорию проекта, вне зависимости от его расположения в системе сервера

                            string pathDelete = (Directory.GetCurrentDirectory() + "/wwwroot/file/" + nametranslit).ToString();
                            System.IO.File.Delete(pathDelete);
                            if (errorMessage != "")
                            {
                                return "Ошибки загрузки следующим пользователям: " + errorMessage;
                            }
                            return "Файл успешно загружен всем пользователям";
                        }
                        catch
                        {
                            return "Ошибка загрузки файла";
                        }
                    }
                    else
                    {
                        return "Ни один пользователь не выбран";
                    }
                }
                else
                {
                    return "Название исходного файла слишком длинное!";
                }
            }
            catch
            {
                return "Ошибка обработки исходных данных";
            }


        }
        /// <summary>
        /// Метод отвечающий за корректное отображение представления с добавлением документов
        /// </summary>
        /// <returns>Представление добавления документов пользователя со списком пользователей</returns>

        [Route("CreateMyDocument")]
        public async Task<IActionResult> Index_Doc()
        {
            if (HttpContext.Session.GetString("Id_User")! == null)
            {
                return RedirectToAction("Index_Auth");
            }

            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();
            ViewBag.User = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio).ToString();

            return View(await db.UsersAllInfos.ToListAsync());
        }
        /// <summary>
        /// Метод отвечающий за корректное отображение профиля текущего пользователя системы
        /// </summary>
        /// <returns>Представление профиля пользователя</returns>
        [Route("MyProfile")]
        [HttpGet]
        public async Task<IActionResult> Index_Profile()
        {
            if (HttpContext.Session.GetString("Id_User")! == null)
            {
                return RedirectToAction("Index_Auth");
            }

            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();


            return View(await db.UsersAllInfos.Where(model => model.IdUsers == Int32.Parse(HttpContext.Session.GetString("Id_User")!)).FirstAsync());
        }
        /// <summary>
        /// Метод отвечающий за корректное отображение представления регистрации в системе
        /// </summary>
        /// <returns>Представление регистрации пользователя</returns>
        [Route("UsersRegistration")]
        public IActionResult Index_Register()
        {
            return View();
        }
        /// <summary>
        /// Метод отвечающий за корректное отображение представления с созданием Индивидуального плана работы преподавателя
        /// </summary>
        /// <returns>Представление составления ИПРП</returns>
        [Route("CreateMyPlane")]
        public IActionResult Index_Plane()
        {
            if (HttpContext.Session.GetString("Id_User") == null)
            {
                return RedirectToAction("Index_Auth");
            }
            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();

            return View();
        }
        /// <summary>
        /// Метод отвечающий за корректное отображение представления с всеми пользователями системы. Принимаемый параметр SearchText отвечает за текст, который позволяет
        /// провести поиск по пользователям по частичному вхождению поискового слова. Метод Sort отвечает за сортировку массива по ФИО пользователя
        /// </summary>
        /// <param name="SearchText">Поисковый запрос</param>
        /// <returns>Представление с пользователями системы</returns>

        [Route("AllUsers")]
        [HttpGet]
        public async Task<IActionResult> Index_AllUsers(string SearchText)
        {
            if (HttpContext.Session.GetString("Id_User")! == null)
            {
                return RedirectToAction("Index_Auth");
            }
            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();

            if ((db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId) == 2)
            {
                return RedirectToAction("Index_MyDocument");
            }
            if (SearchText != null)
            {
                var users = await db.UsersWithStatuses.Where(a => a.UsersInfoFio.Contains(SearchText) && a.UsersSId != int.Parse(HttpContext.Session.GetString("Id_User")!)).Distinct().ToListAsync();
                users.Sort(((a, b) => a.UsersInfoFio.CompareTo(b.UsersInfoFio)));
                return View(users);
            }
            else
            {
                var users = await db.UsersWithStatuses.Where(a => a.UsersSId != int.Parse(HttpContext.Session.GetString("Id_User")!)).Distinct().ToListAsync();
                users.Sort(((a, b) => a.UsersInfoFio.CompareTo(b.UsersInfoFio)));
                return View(users);
            }
        }
        /// <summary>
        /// Метод отвечающий за корректное отображение представления с всеми документами выбранного пользователя системы. Принимаемый параметр SearchText отвечает за текст, который позволяет
        /// провести поиск по документам по частичному вхождению поискового слова. Метод Sort отвечает за сортировку массива файлов. DiskCheckerAsync отвечает за синхронизацию файлов
        /// </summary>
        /// <param name="IdUser">ID пользователя</param>
        /// <param name="SearchText">Поисковый запрос</param>
        /// <returns>Представление с документами выбранного пользователя</returns>
        [Route("AllUsers/Portfolio")]
        [HttpPost]
        public async Task<IActionResult> Index_User_Portfolio(int IdUser, string SearchText)
        {
            if (HttpContext.Session.GetString("Id_User")! == null)
            {
                return RedirectToAction("Index_Auth");
            }
            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();

            if ((db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId) == 2)
            {
                return RedirectToAction("Index_MyDocument");
            }

            if (SearchText != null)
            {
                ViewBag.UsersFio = db.UsersAllInfos.Where(model => model.IdUsers == IdUser).Single().UsersInfoFio;
                ViewBag.UsersId = db.UsersAllInfos.Where(model => model.IdUsers == IdUser).Single().IdUsers;
                var portfolio = await db.DocumentUsers.Where(model => model.UsersDocId == IdUser && model.DocumentsAllName.Contains(SearchText)).ToListAsync();
                portfolio.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                return View(portfolio);
            }
            else
            {

                await DiskCheckerAsync(IdUser);
                ViewBag.UsersFio = db.UsersAllInfos.Where(model => model.IdUsers == IdUser).Single().UsersInfoFio;
                ViewBag.UsersId = db.UsersAllInfos.Where(model => model.IdUsers == IdUser).Single().IdUsers;
                var portfolio = await db.DocumentUsers.Where(model => model.UsersDocId == IdUser).ToListAsync();
                portfolio.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                return View(portfolio);
            }
        }
        /// <summary>
        /// Метод отвечающий за смену ФИО пользователя. ФИО изменяется как в БД, так и на Яндекс диск. При добавлении документа в названии
        /// будет использоваться начальная фамилия, при которой пользователь регистрировался
        /// </summary>
        /// <param name="Name">Имя пользователя</param>
        /// <param name="Fam">Фамилия пользователя</param>
        /// <param name="Ot">Отчество пользователя</param>
        /// <returns>Представление профиля</returns>
        [HttpPost]
        public async Task<IActionResult> ChangeFIO(string Name, string Fam, string Ot)
        {
            try
            {
                // Обновление данных пользователя в БД
                var updateFIO = db.UsersInfos.Find(Int32.Parse(HttpContext.Session.GetString("Id_User")!));
                string oldPathUser = updateFIO!.UsersInfoFio;
                string newPathUser = Fam + " " + Name + " " + Ot;
                updateFIO.UsersInfoFio = newPathUser;
                db.Entry(updateFIO).State = EntityState.Modified;
                db.SaveChanges();
                // Работа с данными пользователя на Яндекс диск. Переименование папки осуществляется перемещением данной папки в эту же дирректорию
                // но с новым наименованием. Любое изменение осуществляется с помощью перемещения файла
                string oauthToken = _configuration.GetConnectionString("TokenAuth")!;
                var api = new DiskHttpApi(oauthToken);
                IDiskApi diskApi = new DiskHttpApi(oauthToken);

                var request = new MoveFileRequest
                {
                    From = "портфолио преподавателей/" + oldPathUser,
                    Path = "портфолио преподавателей/" + newPathUser,
                    Overwrite = false
                };
                await diskApi.Commands.MoveAndWaitAsync(
                    request: request,
                    cancellationToken: CancellationToken.None,
                    pullPeriod: null
                   );
                return RedirectToAction("Index_Profile");
            }
            catch
            {
                return RedirectToAction("Index_Profile");
            }
        }
        /// <summary>
        /// Метод отвечающий за проверку синхронизации файлов как на Яндекс диске, так и в базе данных. Лишние или некорректные данные удаляются.
        /// Данные пользователя проверяются в области 10000 файлов. Если необходимо иное число, необходимо изменить данные в параметре limit в url
        /// для запроса к API Яндекс диска. Полученные данные парсятся в формат JObject, что позволяет провести перебор по кажбому из компонентов ответа.
        /// </summary>
        /// <param name="IdUser">ID пользователя</param>
        /// <returns></returns>
        public async System.Threading.Tasks.Task DiskCheckerAsync(int IdUser)
        {
            string FIO = (db.UsersInfos.Where(a => a.UsersSId == IdUser).Single().UsersInfoFio).ToString();
            string token = _configuration.GetConnectionString("TokenAuth")!;
            var api = new DiskHttpApi(token);
            string url = "https://cloud-api.yandex.net/v1/disk/resources?path=/портфолио преподавателей/" + FIO + "/&limit=10000";
            string url2 = "";
            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("OAuth", token);
                HttpResponseMessage response = await httpClient.GetAsync(url);
                string responseBody = await response.Content.ReadAsStringAsync();
                var json = JObject.Parse(responseBody);
                dynamic jsonSearch = JObject.Parse(responseBody);
                var documents = db.DocumentsAlls.Where(a => (a.UsersDocId == IdUser)).Distinct().ToList();

                foreach (var doc in documents)
                {
                    bool containsObject = json["_embedded"]!["items"]!.Any(item => item["name"]!.ToString() == doc.DocumentsAllName);
                    if (containsObject == false)
                    {
                        var deleteDoc = db.DocumentsAlls.Find(doc.IdDocumentsAll);
                        if (deleteDoc != null)
                        {
                            db.DocumentsAlls.Remove(deleteDoc);
                            db.SaveChanges();
                        }

                    }
                    else
                    {

                    }

                }
                //Удаление файлов с Яндекс диска осуществляется методом обращения к ресурсам (resources) методом DELETE. Чтобы файлы не висели мертвым грузом в корзине
                //в url запроса добавлен параметр permanently=true.Для изменения этой функции необходимо изменить параметр на false.
                //Чтобы не было ошибок в передачи данных, название файла переводится в URL формат с помощью команды HttpUtility.UrlEncode. 
                foreach (var item in json["_embedded"]!["items"]!)
                {
                    bool exists = documents.Any(x => x.DocumentsAllName == item["name"]!.ToString());
                    if (exists == false)
                    {
                        string encod = HttpUtility.UrlEncode(item["name"]!.ToString());

                        url2 = "https://cloud-api.yandex.net/v1/disk/resources?path=/портфолио преподавателей/" + FIO + "/" + encod + "&permanently=true";
                        HttpResponseMessage response2 = await httpClient.DeleteAsync(url2);

                        string responseBody2 = await response2.Content.ReadAsStringAsync();
                        Debug.WriteLine("Файл удален " + item["name"]!.ToString());

                    }
                    else
                    {

                    }
                }

            }

        }
        /// <summary>
        /// Метод отвечающий за корректное отображение представления с всеми документами текущего пользователя системы. Принимаемый параметр SearchText отвечает за текст, который позволяет
        /// провести поиск по документам по частичному вхождению поискового слова. Метод Sort отвечает за сортировку массива файлов. DiskCheckerAsync отвечает за синхронизацию файлов.
        /// Файлы имеющие тип Сгенерировано (тип 100) не выводятся в данном представлении, так как они не относятся на прямую к портфолио
        /// </summary>
        /// <param name="SearchText">Поисковый запрос</param>
        /// <returns>Представление с документами текущего пользователя</returns>
        [Route("MyDocument")]
        [HttpGet]
        public async Task<IActionResult> Index_MyDocument(string SearchText)
        {
            if (HttpContext.Session.GetString("Id_User")! == null)
            {
                return RedirectToAction("Index_Auth");
            }
            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();

            if (SearchText != null)
            {
                var portfolio = await db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentType != 100 && model.DocumentsAllName.Contains(SearchText)).ToListAsync();
                portfolio.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                return View(portfolio);
            }
            else
            {

                await DiskCheckerAsync(int.Parse(HttpContext.Session.GetString("Id_User")!));


                var portfolio = await db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentType != 100).ToListAsync();
                portfolio.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                return View(portfolio);

            }

        }
        /// <summary>
        /// Метод отвечающий за корректное отображение представления с всеми сгенерированными ранее документами выбранного пользователя системы. Принимаемый параметр SearchText отвечает за текст, который позволяет
        /// провести поиск по документам по частичному вхождению поискового слова. Метод Sort отвечает за сортировку массива файлов. DiskCheckerAsync отвечает за синхронизацию файлов.
        /// Файлы имеющие иной тип, кроме тип Сгенерировано (тип 100) не выводятся в данном представлении, так как они не относятся к сгенерированным системой файлам
        /// </summary>
        /// <param name="SearchText">Поисковый запрос</param>
        /// <returns>Представление со всеми сгенерированными документами текущего пользователя</returns>

        [Route("MyGenerations")]
        [HttpGet]
        public async Task<IActionResult> Index_MyGenerate(string SearchText)
        {

            if (HttpContext.Session.GetString("Id_User")! == null)
            {
                return RedirectToAction("Index_Auth");
            }

            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();
            if (SearchText != null)
            {
                var portfolio = await db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentType == 100 && model.DocumentsAllName.Contains(SearchText)).ToListAsync();
                portfolio.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                return View(portfolio);
            }
            else
            {
                await DiskCheckerAsync(int.Parse(HttpContext.Session.GetString("Id_User")!));
                var portfolio = await db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentType == 100).ToListAsync();
                portfolio.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                return View(portfolio);

            }
        }
        /// <summary>
        /// Метод отвечающий за корректное отображение представления авторизации в системе 
        /// </summary>
        /// <returns>Представление авторизации в системе</returns>
        public IActionResult Index_Auth()
        {
            return View();


        }
        /// <summary>
        /// Метод отвечающий за получение шаблона Информационной справки преподавателя на получение высшей категории. Итоговый полученный шаблон является шаблоном в формате DocX
        /// </summary>
        /// <returns>Шаблон файла Информационная справка на получение высшей категории в формате DocX</returns>
        private DocX GetRejectionLetterTemplate2()
        {
            string fileName = Directory.GetCurrentDirectory() + "/wwwroot/templates/templates2.docx";
            var doc = DocX.Load(fileName);
            return doc;
        }
        /// <summary>
        /// Метод отвечающий за получение шаблона Информационной справки преподавателя на получение первой категории. Итоговый полученный шаблон является шаблоном в формате DocX
        /// </summary>
        /// <returns>Шаблон файла Информационная справка на получение первой категории в формате DocX</returns>
        private DocX GetRejectionLetterTemplate3()
        {
            string fileName = Directory.GetCurrentDirectory() + "/wwwroot/templates/templates3.docx";
            var doc = DocX.Load(fileName);
            return doc;
        }
        /// <summary>
        /// Метод отвечающий за создание Информационной справки преподавателя на получение Высшей квалификационной категории. Информационная справка создается по изначальному шаблону,
        /// предоставленному образовательной организацией
        /// </summary>
        /// <returns>Статус создания справки в формате bool</returns>
        public async Task<bool> CreateInformAsync()
        {

            try
            {
                //В название при создании добавляется фамилия пользователя. Из-за специфики сервера (разрешено только добавление файлов с англоязычными названиями), фамилия
                //пользователя проходит транслитерацию на английский язык
                TranslitMethods.Translitter trn = new TranslitMethods.Translitter();
                string lastName = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoLastName;
                surname = trn.Translit(lastName, TranslitMethods.TranslitType.Gost).ToString();

                string fileNameTemplate = Directory.GetCurrentDirectory() + "/wwwroot/file/{0}.templates2.docx";

                //Перебор данных для заполнения Критерия I. В данный критерий входят Сводные ведомости за межаттестационный период, а также набор дисциплин
                //которые ведет преподаватель в зависимости от загруженных ведомостей
                string outputFileName = string.Format(fileNameTemplate, surname);
                int i = 0;
                int iteration = 0;
                int i2 = 0;
                int numbermax = -1;
                int numbermin = -1;
                string ved = "";
                string ved2 = "";
                string mdkstr = "";
                DateTime dateKateg = DateTime.Parse(db.UsersAllInfos.Where(a => a.IdUsers == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().KategoryDate.ToString()!);
                string? date = dateKateg.ToString("dd MM yyyy");
                string replace = date.Substring(0, 3) + Month[Int32.Parse(date.Substring(3, 2))] + date.Substring(5, 5);
                string dateStart = replace;
                int year = int.Parse(date.Substring(6, 4)) - 5;
                dateStart = dateStart.Replace(date.Substring(6, 4), year.ToString());
                DocX letter = GetRejectionLetterTemplate2();
                var documents = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 31) && (a.DocumentUnique == 1))).Distinct().ToList();
                documents.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));

                List<string> mdk = new List<string>();
                foreach (var doc in documents)
                {
                    if (int.Parse(doc.Parametr3!.Substring(0, 4)) >= year)
                    {
                        if (numbermax == -1 || numbermin == -1)
                        {
                            numbermax = i;
                            numbermin = i;
                        }
                        mdk.Add(documents[i].Parametr4!);
                        ved = ved + documents[i].Parametr3 + " уч. год - процент качества " + documents[i].Parametr6 + "%, средний балл " + documents[i].Parametr5 + ";" + "\n         ";
                        ved2 = ved2 + "1." + (iteration + 1).ToString() + ". Сводная ведомость успеваемости(" + documents[i].Parametr3 + "); ";

                        numbermax = i;
                        iteration++;
                    }
                    else
                    {

                    }
                    i++;
                }
                List<string> distinct = mdk.Distinct().ToList();
                foreach (var md in distinct)
                {
                    mdkstr = mdkstr + distinct[i2] + ", ";
                    i2++;
                }
                string dist = "";
                if (distinct.Count <= 1)
                {
                    dist = "ы";
                }
                //Перебор данных для заполнения Критерия II. В данный критерий входят Сдачи студентами сетификационных экзаменов за межаттестационный период,а также их внеучебные достижения

                var sertificationAll = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 7 || a.DocumentType == 8 || a.DocumentType == 9 || a.DocumentType == 40) && (a.DocumentUnique == 1))).Distinct().ToList();
                sertificationAll.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                var sertification = sertificationAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 8) && (a.DocumentUnique == 1))).Distinct().ToList();
                string statistic = "";
                string sertDocument = "";
                int numberIteration = 1;
                int lastIndex = 0;
                int selection = 1;
                foreach (var sert in sertification)
                {
                    if (int.Parse(sert.Parametr1!.Substring(6, 4)) >= year)
                    {
                        if (statistic == "")
                        {
                            statistic = numberIteration.ToString() + ". Успешная сдача сертификационных экзаменов по результатам обучения преподаваемых дисциплин: \r\n";
                        }
                        lastIndex = sert.DocumentsAllName.LastIndexOf(".");
                        sertDocument = sertDocument + "         - Сертификационный экзамен от " + sert.Parametr2 + " «" + sert.Parametr3 + "». Студент " + sert.Parametr4 + " (2." + numberIteration.ToString() + "." + selection.ToString() + ". " + sert.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";


                        selection++;
                    }

                }
                if (statistic != "")
                {
                    statistic = statistic + sertDocument;
                    sertDocument = "";
                    numberIteration++;
                    selection = 1;
                }
                sertification = sertificationAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && ((a.DocumentType == 7) || (a.DocumentType == 9) || (a.DocumentType == 40)) && (a.DocumentUnique == 1))).Distinct().ToList();
                string statistic2 = "";
                foreach (var sert in sertification)
                {
                    if (int.Parse(sert.Parametr1!.Substring(6, 4)) >= year)
                    {
                        if (statistic2 == "")
                        {
                            statistic2 = "         " + numberIteration.ToString() + ". Эффективность педагогической деятельности подтверждается внеучебными достижениями студентов: \n";
                        }
                        if (sert.DocumentType == 7)
                        {

                            lastIndex = sert.DocumentsAllName.LastIndexOf(".");
                            sertDocument = sertDocument + "         - Участие в мероприятии от " + sert.Parametr2 + " в номинации " + " «" + sert.Parametr4 + "» cтудентом " + sert.Parametr5 + " (2." + numberIteration.ToString() + "." + selection.ToString() + ". " + sert.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        else if (sert.DocumentType == 9)
                        {
                            lastIndex = sert.DocumentsAllName.LastIndexOf(".");
                            sertDocument = sertDocument + "         - Участие студента в мероприятии от WorldSkills Russia в номинации " + " «" + sert.Parametr3 + "» cтудентом " + sert.Parametr4 + " (2." + numberIteration.ToString() + "." + selection.ToString() + ". " + sert.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        else if (sert.DocumentType == 40)
                        {
                            lastIndex = sert.DocumentsAllName.LastIndexOf(".");
                            sertDocument = sertDocument + "         - Участие студента в мероприятии от Digital Skills в номинации " + " «" + sert.Parametr3 + "» cтудентом " + sert.Parametr4 + " (2." + numberIteration.ToString() + "." + selection.ToString() + ". " + sert.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }

                        selection++;
                    }

                }
                if (statistic2 != "")
                {
                    statistic2 = statistic2 + sertDocument;
                    sertDocument = "";
                    numberIteration++;
                    selection = 1;
                }
                //Перебор данных для заполнения Критерия III. В данный критерий входят участия в WSR и DigitalSkills, подготовка студентов к участию в олимпиаде, учебные практики,
                //производственные практики, руководство выпускными квалификационными работами, а также ведение курсовых проектов студентов за межаттестационный период.
                numberIteration = 1;
                var orderAll = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
               (((a.DocumentType == 41) && (a.Parametr2 != "Участник")) ||
               ((a.DocumentType == 10) && (a.Parametr2 != "Участник")) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("по подготовке") && a.Parametr3.Contains("олимпиаде"))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("учебные") || a.Parametr3.Contains("учебную") || a.Parametr3.Contains("УП ") || a.Parametr3.Contains("УП)") || a.DocumentsAllName.Contains("УП."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("производственную") || a.Parametr3.Contains("ПП ") || a.Parametr3.Contains("ПП)") || a.DocumentsAllName.Contains("ПП."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("выпускных") || a.Parametr3.Contains("ВКР ") || a.Parametr3.Contains("ВКР)") || a.DocumentsAllName.Contains("ВКР."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("курсовой") || a.Parametr3.Contains("курсовых") || a.Parametr3.Contains("курсового") || a.Parametr3.Contains("КП ") || a.Parametr3.Contains("КП)") || a.DocumentsAllName.Contains("КП.")))
               ))).Distinct().ToList();
                orderAll.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                var order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("курсовой") || a.Parametr3.Contains("курсовых") || a.Parametr3.Contains("курсового") || a.Parametr3.Contains("КП ") || a.Parametr3.Contains("КП)") || a.DocumentsAllName.Contains("КП.")))).Distinct().ToList();

                string ordersInfo = "";
                foreach (var ord in order)
                {
                    if (int.Parse(ord.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = ord.DocumentsAllName.LastIndexOf(".");
                        if (int.Parse(ord.Parametr1.Substring(3, 2)) >= 8)
                        {
                            int yearYch = int.Parse(ord.Parametr1.Substring(6, 4)) + 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году являлся руководителем или консультантом по выполнению курсовых проектирований студентов. " + " (3." + selection.ToString() + ".1. " + ord.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        else if (int.Parse(ord.Parametr1.Substring(3, 2)) < 8)
                        {
                            int yearYch = int.Parse(ord.Parametr1.Substring(6, 4)) - 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord.Parametr1.Substring(6, 4) + " учебном году являлся руководителем или консультантом по выполнению курсовых проектирований студентов. " + " (3." + selection.ToString() + ".1. " + ord.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        selection++;

                    }
                }
                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("выпускных") || a.Parametr3.Contains("ВКР ") || a.Parametr3.Contains("ВКР)") || a.DocumentsAllName.Contains("ВКР.")))).Distinct().ToList();

                foreach (var ord2 in order)
                {
                    if (int.Parse(ord2.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = ord2.DocumentsAllName.LastIndexOf(".");
                        if (int.Parse(ord2.Parametr1.Substring(3, 2)) >= 8)
                        {
                            int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) + 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году являлся руководителем или консультантом по выполнению выпускных квалификационных работы обучающихся. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        else if (int.Parse(ord2.Parametr1.Substring(3, 2)) < 8)
                        {
                            int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) - 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord2.Parametr1.Substring(6, 4) + " учебном году являлся руководителем или консультантом по выполнению выпускных квалификационных работы обучающихся. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        selection++;

                    }
                }

                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("производственную") || a.Parametr3.Contains("ПП ") || a.Parametr3.Contains("ПП)") || a.DocumentsAllName.Contains("ПП.")))).Distinct().ToList();

                foreach (var ord2 in order)
                {
                    if (int.Parse(ord2.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = ord2.DocumentsAllName.LastIndexOf(".");
                        if (int.Parse(ord2.Parametr1.Substring(3, 2)) >= 8)
                        {
                            int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) + 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году осуществлял руководство студентами в рамках производственной практики. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        else if (int.Parse(ord2.Parametr1.Substring(3, 2)) < 8)
                        {
                            int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) - 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord2.Parametr1.Substring(6, 4) + " учебном году осуществлял руководство студентами в рамках производственной практики. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        selection++;

                    }
                }


                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("учебные") || a.Parametr3.Contains("учебную") || a.Parametr3.Contains("УП ") || a.Parametr3.Contains("УП)") || a.DocumentsAllName.Contains("УП.")))).Distinct().ToList();

                foreach (var ord2 in order)
                {
                    if (int.Parse(ord2.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = ord2.DocumentsAllName.LastIndexOf(".");
                        if (int.Parse(ord2.Parametr1.Substring(3, 2)) >= 8)
                        {
                            int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) + 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году осуществлял руководство студентами в рамках учебной практики. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        else if (int.Parse(ord2.Parametr1.Substring(3, 2)) < 8)
                        {
                            int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) - 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord2.Parametr1.Substring(6, 4) + " учебном году осуществлял руководство студентами в рамках учебной практики. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        selection++;

                    }
                }

                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("по подготовке") && a.Parametr3.Contains("олимпиаде")))).Distinct().ToList();

                foreach (var ord2 in order)
                {
                    if (int.Parse(ord2.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = ord2.DocumentsAllName.LastIndexOf(".");
                        int index = ord2.DocumentsAllName.LastIndexOf("по подготовке") + 13;
                        if (int.Parse(ord2.Parametr1.Substring(3, 2)) >= 8)
                        {
                            int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) + 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году являлся руководителем по подготовке" + ord2.DocumentsAllName.Substring(index, lastIndex - index) + " . " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        else if (int.Parse(ord2.Parametr1.Substring(3, 2)) < 8)
                        {
                            int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) - 1;
                            ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord2.Parametr1.Substring(6, 4) + " учебном году являлся руководителем по подготовке" + ord2.DocumentsAllName.Substring(index, lastIndex - index) + " . " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        }
                        selection++;

                    }
                }
                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 10) && (a.DocumentUnique == 1) && (a.Parametr2 != "Участник"))).Distinct().ToList();
                var student = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 9) && (a.DocumentUnique == 1))).Distinct().ToList();
                student.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                int studNumber = 2;
                foreach (var ord2 in order)
                {
                    if (int.Parse(ord2.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = ord2.DocumentsAllName.LastIndexOf(".");

                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4));
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + " году принимал участие чемпионате по стандартам WorldSkills Russia в роли «" + ord2.Parametr2 + "» по компетенции «" + ord2.Parametr5 + "». (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        foreach (var stud in student)
                        {
                            if (int.Parse(ord2.Parametr1.Substring(6, 4)) == int.Parse(stud.Parametr1!.Substring(6, 4)))
                            {
                                if (ord2.Parametr5 == stud.Parametr3)
                                {
                                    ordersInfo = ordersInfo + "          - Участник " + stud.Parametr4 + " под руководством принимал участие в чемпионате по стандартам WorldSkills Russia в данной компетенции.";
                                    if (stud.Parametr5 != null)
                                    {
                                        ordersInfo = ordersInfo + " Место занятое студентом по результатам чемпионата: " + stud.Parametr5 + ". ";
                                    }
                                    lastIndex = stud.DocumentsAllName.LastIndexOf(".");
                                    ordersInfo = ordersInfo + " (3." + selection.ToString() + "." + studNumber.ToString() + ". " + stud.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                                    studNumber++;
                                }
                            }

                        }
                        selection++;
                        studNumber = 2;

                    }
                }
                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 41) && (a.DocumentUnique == 1) && (a.Parametr2 != "Участник"))).Distinct().ToList();
                student = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 40) && (a.DocumentUnique == 1))).Distinct().ToList();

                student.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                studNumber = 2;
                foreach (var ord2 in order)
                {
                    if (int.Parse(ord2.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = ord2.DocumentsAllName.LastIndexOf(".");

                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4));
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + " году принимал участие чемпионате DigitalSkills в роли «" + ord2.Parametr2 + "» по компетенции «" + ord2.Parametr5 + "». (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        foreach (var stud in student)
                        {
                            if (int.Parse(ord2.Parametr1.Substring(6, 4)) == int.Parse(stud.Parametr1!.Substring(6, 4)))
                            {
                                if (ord2.Parametr5 == stud.Parametr3)
                                {
                                    ordersInfo = ordersInfo + "          - Участник " + stud.Parametr4 + " под руководством принимал участие в чемпионате DigitalSkills в данной компетенции.";
                                    if (stud.Parametr5 != null)
                                    {
                                        ordersInfo = ordersInfo + " Место занятое студентом по результатам чемпионата: " + stud.Parametr5 + ". ";
                                    }
                                    lastIndex = stud.DocumentsAllName.LastIndexOf(".");
                                    ordersInfo = ordersInfo + " (3." + selection.ToString() + "." + studNumber.ToString() + ". " + stud.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                                    studNumber++;
                                }
                            }

                        }
                        selection++;
                        studNumber = 2;

                    }
                }
                selection = 1;

                //Перебор данных для заполнения Критерия IV. В данный критерий входят участия преподавателя в различных мероприятиях по типу Вебинар, Семинар..., а также организация дней открытых дверей за межаттестационный период.
                var individualAll = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
              ((a.DocumentType == 24) ||
              (a.DocumentType == 17) ||
              (a.DocumentType == 2) ||
              (a.DocumentType == 3) ||
              (a.DocumentType == 1) ||
              (a.DocumentType == 12) ||
              (a.DocumentType == 13) ||
              (a.DocumentType == 22) ||
              (a.DocumentType == 23) ||
              ((a.DocumentType == 15) && (a.Parametr3!.Contains("день открытых дверей") || a.Parametr3.Contains("дня открытых дверей") || a.Parametr3.Contains("День открытых дверей"))) ||
              (a.DocumentType == 4) ||
              (a.DocumentType == 5) ||
              (a.DocumentType == 21) ||
              (a.DocumentType == 20) ||
              (a.DocumentType == 19) ||
              (a.DocumentType == 18)
              ))).Distinct().ToList();
                individualAll.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                string individualParametr = "";
                var individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 24) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        individualParametr = individualParametr + "          " + selection.ToString() + ". В " + individ.Parametr1.Substring(6, 4) + " принимал участие в работе круглого стола, организованного " + individ.Parametr2 + " на тему: «" + individ.Parametr3 + "». (4." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        selection++;
                    }
                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 17) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        individualParametr = individualParametr + "          " + selection.ToString() + ". В " + individ.Parametr1.Substring(6, 4) + " принимал участие в конференции, организованной " + individ.Parametr2 + " на тему: «" + individ.Parametr3 + "». (4." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        selection++;
                    }
                }
                string skill = "";
                int skillCount = 1;
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && ((a.DocumentType == 2) && (a.DocumentType == 3)) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    if ((int.Parse(individ.Parametr1!.Substring(6, 4))) >= year && (individ.DocumentType == 3))
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Диплом о высшем образовании по специальности «" + individ.Parametr3 + "» в «" + individ.Parametr2 + "» с присвоением квалификации «" + individ.Parametr4 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }
                    if ((int.Parse(individ.Parametr1.Substring(6, 4))) >= year && (individ.DocumentType == 2))
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Диплом о среднем профессиональном образовании по специальности «" + individ.Parametr3 + "» в «" + individ.Parametr2 + "» с присвоением квалификации «" + individ.Parametr4 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }
                }

                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 1) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Диплом о начальном профессиональном образовании в «" + individ.Parametr2 + "» с присвоением квалификации «" + individ.Parametr3 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 12) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Диплом о профессиональной переподготовке по программе «" + individ.Parametr3 + "» в «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 13) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Удостоверение о повышении квалификации по программе «" + individ.Parametr3 + "» в «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 22) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Сертификат участника вебинара на тему  «" + individ.Parametr3 + "» организованного «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 23) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Сертификат участника семинара на тему  «" + individ.Parametr3 + "» организованного «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 18) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Сертификат участника мастер-класса на тему  «" + individ.Parametr3 + "» организованного «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 19) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Сертификат участника форума на тему  «" + individ.Parametr3 + "» организованного «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 20) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Сертификат участника панельной дискуссии на тему  «" + individ.Parametr3 + "» организованной «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 21) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Сертификат участника онлайн-класса на тему  «" + individ.Parametr3 + "» организованной «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 32) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Стажировка по программе  «" + individ.Parametr3 + "» на базе «" + individ.Parametr2 + "». (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 5) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Сертификационный экзамен по теме  «" + individ.Parametr3 + "» от «" + individ.Parametr2 + "». (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 4) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Вендорный сертификационный экзамен по теме  «" + individ.Parametr3 + "» от «" + individ.Parametr2 + "». (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }

                }
                if (skill != "")
                {

                    individualParametr = individualParametr + "          " + selection.ToString() + ". За межаттестационный период постоянно повышал свой профессиональный опыт, получил следующие дипломы, удостоверения и сертификаты: \r\n" + skill;
                    selection++;
                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("открытых дверей")))).ToList();
                individual.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        individualParametr = individualParametr + "          " + selection.ToString() + ". В " + individ.Parametr1.Substring(6, 4) + " году принимал участие в проведении профориентационного мероприятия «День открытых дверей» в РЭУ им. Г.В. Плеханова. (4." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        selection++;

                    }
                }
                selection = 1;
                //Перебор данных для заполнения Критерия V. В данный критерий входят участие преподавателя в составах комиссий, ЦМК, ГЭК, а также участие в разработке УМК за межаттестационный период
                var activeAll = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
              ((a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("утверждении состава") || a.Parametr3.Contains("составе") || a.Parametr3.Contains("составах") || a.Parametr3.Contains("комиссиях")) ||
              (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ЦМК") || a.Parametr3.Contains("циклов")) ||
              (a.DocumentType == 16) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ГЭК") || a.Parametr3.Contains("государственных экзаменационных") || a.Parametr3.Contains("государственной экзаменационной")) ||
              (a.DocumentType == 33)
              ))).Distinct().ToList();
                activeAll.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                string active = "";
                individual = activeAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("утверждении состава") || a.Parametr3.Contains("составе") || a.Parametr3.Contains("составах") || a.Parametr3.Contains("комиссиях")))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        int index = 0;
                        if (individ.Parametr3!.Contains("утверждении состава"))
                        {
                            index = individ.DocumentsAllName.LastIndexOf("утверждении состава") + 19;
                        }
                        else if (individ.Parametr3.Contains("составе"))
                        {
                            index = individ.DocumentsAllName.LastIndexOf("составе") + 7;
                        }
                        else if (individ.Parametr3.Contains("составах"))
                        {
                            index = individ.DocumentsAllName.LastIndexOf("составах") + 8;
                        }
                        else if (individ.Parametr3.Contains("комиссиях"))
                        {
                            index = individ.DocumentsAllName.LastIndexOf("комиссиях") + 9;
                        }
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");


                        active = active + "          " + selection.ToString() + ". В " + individ.Parametr1.Substring(6, 4) + " году членом состава" + individ.DocumentsAllName.Substring(index, lastIndex - index) + " Московского приборостроительного техникума. (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                        selection++;

                    }
                }
                individual = activeAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ЦМК") || a.Parametr3.Contains("циклов")))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {

                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        active = active + "          " + selection.ToString() + ". В " + individ.Parametr1.Substring(6, 4) + " году членом состава цикловой методической комиссии Московского приборостроительного техникума. (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        selection++;

                    }
                }
                individual = activeAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 16) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ГЭК") || a.Parametr3.Contains("государственных экзаменационных") || a.Parametr3.Contains("государственной экзаменационной")))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {

                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        active = active + "          " + selection.ToString() + ". В " + individ.Parametr1.Substring(6, 4) + " году членом государственной экзаменационной комиссии для проведения итоговой аттестации выпускников. (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        selection++;

                    }
                }
                individual = activeAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 33) && (a.DocumentUnique == 1))).Distinct().ToList();

                if (individual.Count != 0)
                {
                    active = active + "          " + selection.ToString() + ". Принимал участие в разработке УМК по общепрофессиональным дисциплинам и профессиональным модулям: \r\n";
                }
                //РП / СР / КОС / КИМ / МУ / ПР / ЛР
                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {

                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        if (individ.Parametr3 == "РП")
                        {
                            active = active + "          - В " + individ.Parametr1.Substring(6, 4) + " году рабочая программа для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                        }
                        if (individ.Parametr3 == "СР")
                        {
                            active = active + "          - В " + individ.Parametr1.Substring(6, 4) + " году самостоятельные работы для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                        }
                        if (individ.Parametr3 == "КОС")
                        {
                            active = active + "          - В " + individ.Parametr1.Substring(6, 4) + " году комплект оценочных средств для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                        }
                        if (individ.Parametr3 == "КИМ")
                        {
                            active = active + "          - В " + individ.Parametr1.Substring(6, 4) + " году контрольно-измерительный материал для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                        }
                        if (individ.Parametr3 == "МУ")
                        {
                            active = active + "          - В " + individ.Parametr1.Substring(6, 4) + " году методические указания для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                        }
                        if (individ.Parametr3 == "ПР")
                        {
                            active = active + "          - В " + individ.Parametr1.Substring(6, 4) + " году практические работы для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                        }
                        if (individ.Parametr3 == "ЛР")
                        {
                            active = active + "          - В " + individ.Parametr1.Substring(6, 4) + " году лабораторные работы для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                        }
                        selection++;

                    }
                }
                //Сбор сокращенного ФИО пользователя
                string[] FIO = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio.Split(' ');
                string usersFIO = "";
                if (FIO.Length != 2)
                {
                    usersFIO = FIO[1].Substring(0, 1) + "." + FIO[2].Substring(0, 1) + ". " + FIO[0];
                }
                else
                {
                    usersFIO = FIO[1].Substring(0, 1) + ". " + FIO[0];
                }
                //Составление документа путем вставления собранных данных в созданный ранее шаблон DocX
#pragma warning disable CS0618 // Тип или член устарел
                letter.ReplaceText("%Parameter1%", db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio);
                letter.ReplaceText("%Parameter2%", mdkstr);
                letter.ReplaceText("%Parameter3%", documents[numbermin].Parametr5);
                letter.ReplaceText("%Parameter4%", documents[numbermax].Parametr5);
                letter.ReplaceText("%Parameter5%", documents[numbermin].Parametr6);
                letter.ReplaceText("%Parameter6%", documents[numbermax].Parametr6);
                letter.ReplaceText("%Parameter7%", ved2);
                letter.ReplaceText("%Parameter8%", ved);
                letter.ReplaceText("%Parameter9%", dist);
                letter.ReplaceText("%Parameter10%", dateStart);
                letter.ReplaceText("%Parameter11%", replace);
                letter.ReplaceText("%Parameter12%", statistic + "\r\n" + statistic2);
                letter.ReplaceText("%Parameter13%", ordersInfo);
                letter.ReplaceText("%Parameter14%", individualParametr);
                letter.ReplaceText("%Parameter15%", active);
                letter.ReplaceText("%Parameter16%", usersFIO);
#pragma warning restore CS0618 // Тип или член устарел
                letter.SaveAs(outputFileName);

                //Загрузка данных на Яндекс диск с учетом уникальности файла. В названии файла также присутствует ФИО пользователя системы
                string FolderUser = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio;
                string Name = "Информационная справка " + usersFIO + " (Высшая категория) ";
                int i1 = 1;


                string Altername = Name;
                string extension = "docx";
                string oauthToken = _configuration.GetConnectionString("TokenAuth")!;
                var api = new DiskHttpApi(oauthToken);
                IDiskApi diskApi = new DiskHttpApi(oauthToken);



                var usersdoc = db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentsAllName.Contains(Name + "." + extension) && model.DocumentUnique == 1 && model.DocumentType == 100).ToListAsync();
                if (usersdoc.Result.Count == 0)
                {


                    DocumentsAll newDocument = new DocumentsAll
                    {
                        DocumentsAllName = Name + "." + extension,
                        DocumentType = 100,
                        UsersDocId = int.Parse(HttpContext.Session.GetString("Id_User")!),
                        DocumentUnique = 1,
                    };
                    db.DocumentsAlls.Add(newDocument);
                    db.SaveChanges();


                }
                else
                {
                    var contains = db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentUnique == 0 && model.DocumentType == 100).Distinct().ToList();

                    while (true)
                    {
                        Altername = Name + "(" + i1 + ")";
                        var usersdoc2 = contains.Where(model => model.DocumentsAllName.Contains(Altername + "." + extension)).Distinct().ToList();

                        if (usersdoc2.Count == 0)
                        {

                            DocumentsAll Doc6 = new DocumentsAll { };

                            Doc6 = new DocumentsAll
                            {

                                DocumentsAllName = Altername + "." + extension,
                                DocumentType = 100,
                                UsersDocId = int.Parse(HttpContext.Session.GetString("Id_User")!),
                                DocumentUnique = 0



                            };
                            db.DocumentsAlls.Add(Doc6);
                            db.SaveChanges();
                            break;
                        }

                        i1++;
                    }


                }


                if (i1 != 0)
                {
                }

                db.SaveChanges();

                await diskApi.Files.UploadFileAsync(path: "портфолио преподавателей/" + FolderUser + "/" + Altername + ".txt",

                                                    overwrite: false,
                                                    localFile: Directory.GetCurrentDirectory() + "/wwwroot/file/" + surname + ".templates2.docx",

                                                    cancellationToken: CancellationToken.None);




                var request = new MoveFileRequest
                {
                    From = "портфолио преподавателей/" + FolderUser + "/" + Altername + ".txt",
                    Path = "портфолио преподавателей/" + FolderUser + "/" + Altername + "." + extension,
                    Overwrite = false
                };
                await diskApi.Commands.MoveAndWaitAsync(
                    request: request,
                    cancellationToken: CancellationToken.None,
                    pullPeriod: null
                   );
                string pathDelete = (Directory.GetCurrentDirectory() + "/wwwroot/file/" + surname + ".templates2.docx").ToString();
                System.IO.File.Delete(pathDelete);


                return true;
            }
            catch { return false; }

        }
        /// <summary>
        /// Данный метод служит для проверки того, какие файлы необходимы пользователю для формирования информационной справки о высшей категории преподавателя
        /// метод собирает данные о наличии и выводит сообщение пользователю в виде всплывающего сообщения на экране.
        /// </summary>
        /// <returns>Сообщение о не достающей информации</returns>
        public string InfoChecker()
        {
            string message = "";
            string dateCheck = db.UsersAllInfos.Where(a => a.IdUsers == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().KategoryDate.ToString()!;

            if (dateCheck == "")
            {
                message = message + " данные о текущей категории в профиле;";
            }
            else
            {
                DateTime dateKateg = DateTime.Parse(dateCheck);
                string? date = dateKateg.ToString("dd MM yyyy");
                int year = int.Parse(date.Substring(6, 4)) - 5;
                int text = 0;
                var documents = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 31) && (a.DocumentUnique == 1))).Distinct().ToList();
                documents.Sort((a, b) => a.Parametr3!.CompareTo(b.Parametr3));
                foreach (var doc in documents)
                {
                    if (int.Parse(doc.Parametr3!.Substring(0, 4)) >= year)
                    {
                        text = 1;
                        break;
                    }
                }
                if (text != 1 || documents == null)
                {
                    message = message + " отсутствуют сводные ведомости;";
                }
                else { text = 0; }

                var sertification = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 7 || a.DocumentType == 8 || a.DocumentType == 9 || a.DocumentType == 40) && (a.DocumentUnique == 1))).Distinct().ToList();
                sertification.Sort((a, b) => a.Parametr1!.CompareTo(b.Parametr1));
                foreach (var sert in sertification)
                {
                    if (int.Parse(sert.Parametr1!.Substring(6, 4)) >= year)
                    {
                        text = 1;
                        break;
                    }
                }


                if (text != 1 || sertification == null)
                {
                    message = message + " отсутствуют сертификации студентов и/или внеучебные достижения студентов;";
                }
                else { text = 0; }

                var order = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
               (((a.DocumentType == 41) && (a.Parametr2 != "Участник")) ||
               ((a.DocumentType == 10) && (a.Parametr2 != "Участник")) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("по подготовке") && a.Parametr3.Contains("олимпиаде"))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("учебные") || a.Parametr3.Contains("учебную") || a.Parametr3.Contains("УП ") || a.Parametr3.Contains("УП)") || a.DocumentsAllName.Contains("УП."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("производственную") || a.Parametr3.Contains("ПП ") || a.Parametr3.Contains("ПП)") || a.DocumentsAllName.Contains("ПП."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("выпускных") || a.Parametr3.Contains("ВКР ") || a.Parametr3.Contains("ВКР)") || a.DocumentsAllName.Contains("ВКР."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("курсовой") || a.Parametr3.Contains("курсовых") || a.Parametr3.Contains("курсового") || a.Parametr3.Contains("КП ") || a.Parametr3.Contains("КП)") || a.DocumentsAllName.Contains("КП.")))
               ))).Distinct().ToList();

                foreach (var ord in order)
                {
                    if (int.Parse(ord.Parametr1!.Substring(6, 4)) >= year)
                    {
                        text = 1;
                        break;
                    }
                }
                if (text != 1 || order == null)
                {
                    message = message + " отсутствуют данные о кураторстве студентов по УП/ПП/ВКР/КП, олимпиадам или чемпионатам;";
                }
                else { text = 0; }

                var individual = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
                ((a.DocumentType == 24) ||
                (a.DocumentType == 17) ||
                (a.DocumentType == 2) ||
                (a.DocumentType == 3) ||
                (a.DocumentType == 1) ||
                (a.DocumentType == 12) ||
                (a.DocumentType == 13) ||
                (a.DocumentType == 22) ||
                (a.DocumentType == 23) ||
                ((a.DocumentType == 15) && (a.Parametr3!.Contains("день открытых дверей") || a.Parametr3.Contains("дня открытых дверей") || a.Parametr3.Contains("День открытых дверей"))) ||
                (a.DocumentType == 4) ||
                (a.DocumentType == 5) ||
                (a.DocumentType == 21) ||
                (a.DocumentType == 20) ||
                (a.DocumentType == 19) ||
                (a.DocumentType == 18)
                ))).Distinct().ToList();

                foreach (var individ in individual)
                {
                    if (int.Parse(individ.Parametr1!.Substring(6, 4)) >= year)
                    {
                        text = 1;
                        break;
                    }
                }
                if (text != 1 || individual == null)
                {
                    message = message + " отсутствуют данные о получении ВПО/СПО/НПО/ПП/ПК/сертификациях, участии в Круглом столе/конференциях/вебинарах/семинарах и т.д.;";
                }
                else { text = 0; }
                var active = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
               ((a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("утверждении состава") || a.Parametr3.Contains("составе") || a.Parametr3.Contains("составах") || a.Parametr3.Contains("комиссиях")) ||
               (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ЦМК") || a.Parametr3.Contains("циклов")) ||
               (a.DocumentType == 16) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ГЭК") || a.Parametr3.Contains("государственных экзаменационных") || a.Parametr3.Contains("государственной экзаменационной")) ||
               (a.DocumentType == 33)
               ))).Distinct().ToList();

                foreach (var act in active)
                {
                    if (int.Parse(act.Parametr1!.Substring(6, 4)) >= year)
                    {
                        text = 1;
                        break;
                    }
                }
                if (text != 1 || active == null)
                {
                    message = message + " отсутствуют данные об участии в комиссиях и/или разработке УМК;";
                }
                else { text = 0; }
            }
            return message;
        }
        /// <summary>
        /// Метод обработки запроса по формированию информационной справки на получение высшей квалификационной категории
        /// </summary>
        /// <returns>Сообщение о статусе формирования</returns>
        public async Task<string> NewDocumentCreate()
        {
            //DiskCheckerAsync проверяет актуальность данных
            await DiskCheckerAsync(int.Parse(HttpContext.Session.GetString("Id_User")!));
            string result = InfoChecker();
            string message = "";

            if (result == "")
            {
                bool results = await CreateInformAsync();

                if (results == true)
                {
                    message = "Информационная справка успешно сформирована";
                }
                if (results == false)
                {
                    message = "Ошибка формирования документа";
                }
            }
            if (result != "")
            {
                message = "Необходимо добавить в портфолио за межаттестационный период:" + result;

            }
            return message;
        }
        /// <summary>
        /// Метод отвечающий за создание Информационной справки преподавателя на получение Первой квалификационной категории. Информационная справка создается по изначальному шаблону,
        /// предоставленному образовательной организацией
        /// </summary>
        /// <returns>Статус создания справки в формате bool</returns>
        public async Task<bool> CreateInformFirstAsync()
        {

            try
            {
                //В название при создании добавляется фамилия пользователя. Из-за специфики сервера (разрешено только добавление файлов с англоязычными названиями), фамилия
                //пользователя проходит транслитерацию на английский язык
                TranslitMethods.Translitter trn = new TranslitMethods.Translitter();
                string lastName = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoLastName;
                surname = trn.Translit(lastName, TranslitMethods.TranslitType.Gost).ToString();

                string fileNameTemplate = Directory.GetCurrentDirectory() + "/wwwroot/file/{0}.templates3.docx";

                //Перебор данных для заполнения Критерия I. В данный критерий входят все Сводные ведомости, а также набор дисциплин
                //которые ведет преподаватель в зависимости от загруженных ведомостей
                string outputFileName = string.Format(fileNameTemplate, surname);
                int i = 0;
                int iteration = 0;
                int i2 = 0;
                int numbermax = -1;
                int numbermin = -1;
                string ved = "";
                string ved2 = "";
                string mdkstr = "";
                DocX letter = GetRejectionLetterTemplate3();
                var documents = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 31) && (a.DocumentUnique == 1))).Distinct().ToList();
                documents.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));

                List<string> mdk = new List<string>();
                foreach (var doc in documents)
                {

                    if (numbermax == -1 || numbermin == -1)
                    {
                        numbermax = i;
                        numbermin = i;
                    }
                    mdk.Add(documents[i].Parametr4!);
                    ved = ved + documents[i].Parametr3 + " уч. год - процент качества " + documents[i].Parametr6 + "%, средний балл " + documents[i].Parametr5 + ";" + "\n         ";
                    ved2 = ved2 + "1." + (iteration + 1).ToString() + ". Сводная ведомость успеваемости(" + documents[i].Parametr3 + "); ";

                    numbermax = i;
                    iteration++;

                    i++;
                }
                List<string> distinct = mdk.Distinct().ToList();
                foreach (var md in distinct)
                {
                    mdkstr = mdkstr + distinct[i2] + ", ";
                    i2++;
                }
                string dist = "";
                if (distinct.Count <= 1)
                {
                    dist = "ы";
                }
                //Перебор данных для заполнения Критерия II. В данный критерий входят Сдачи студентами сетификационных экзаменов,а также их внеучебные достижения
                var sertificationAll = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 7 || a.DocumentType == 8 || a.DocumentType == 9 || a.DocumentType == 40) && (a.DocumentUnique == 1))).Distinct().ToList();
                sertificationAll.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                var sertification = sertificationAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 8) && (a.DocumentUnique == 1))).Distinct().ToList();
                string statistic = "";
                string sertDocument = "";
                int numberIteration = 1;
                int lastIndex = 0;
                int selection = 1;
                foreach (var sert in sertification)
                {

                    if (statistic == "")
                    {
                        statistic = numberIteration.ToString() + ". Успешная сдача сертификационных экзаменов по результатам обучения преподаваемых дисциплин: \r\n";
                    }
                    lastIndex = sert.DocumentsAllName.LastIndexOf(".");
                    sertDocument = sertDocument + "         - Сертификационный экзамен от " + sert.Parametr2 + " «" + sert.Parametr3 + "». Студент " + sert.Parametr4 + " (2." + numberIteration.ToString() + "." + selection.ToString() + ". " + sert.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";


                    selection++;


                }
                if (statistic != "")
                {
                    statistic = statistic + sertDocument;
                    sertDocument = "";
                    numberIteration++;
                    selection = 1;
                }
                sertification = sertificationAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && ((a.DocumentType == 7) || (a.DocumentType == 9) || (a.DocumentType == 40)) && (a.DocumentUnique == 1))).Distinct().ToList();

                string statistic2 = "";
                foreach (var sert in sertification)
                {

                    if (statistic2 == "")
                    {
                        statistic2 = "         " + numberIteration.ToString() + ". Эффективность педагогической деятельности подтверждается внеучебными достижениями студентов: \n";
                    }
                    if (sert.DocumentType == 7)
                    {

                        lastIndex = sert.DocumentsAllName.LastIndexOf(".");
                        sertDocument = sertDocument + "         - Участие в мероприятии от " + sert.Parametr2 + " в номинации " + " «" + sert.Parametr4 + "» cтудентом " + sert.Parametr5 + " (2." + numberIteration.ToString() + "." + selection.ToString() + ". " + sert.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    else if (sert.DocumentType == 9)
                    {
                        lastIndex = sert.DocumentsAllName.LastIndexOf(".");
                        sertDocument = sertDocument + "         - Участие студента в мероприятии от WorldSkills Russia в номинации " + " «" + sert.Parametr3 + "» cтудентом " + sert.Parametr4 + " (2." + numberIteration.ToString() + "." + selection.ToString() + ". " + sert.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    else if (sert.DocumentType == 40)
                    {
                        lastIndex = sert.DocumentsAllName.LastIndexOf(".");
                        sertDocument = sertDocument + "         - Участие студента в мероприятии от Digital Skills в номинации " + " «" + sert.Parametr3 + "» cтудентом " + sert.Parametr4 + " (2." + numberIteration.ToString() + "." + selection.ToString() + ". " + sert.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }


                    selection++;


                }
                if (statistic2 != "")
                {
                    statistic2 = statistic2 + sertDocument;
                    sertDocument = "";
                    numberIteration++;
                    selection = 1;
                }
                //Перебор данных для заполнения Критерия III. В данный критерий входят участия в WSR и DigitalSkills, подготовка студентов к участию в олимпиаде, учебные практики,
                //производственные практики, руководство выпускными квалификационными работами, а также ведение курсовых проектов студентов.
                numberIteration = 1;
                var orderAll = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
               (((a.DocumentType == 41) && (a.Parametr2 != "Участник")) ||
               ((a.DocumentType == 10) && (a.Parametr2 != "Участник")) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("по подготовке") && a.Parametr3.Contains("олимпиаде"))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("учебные") || a.Parametr3.Contains("учебную") || a.Parametr3.Contains("УП ") || a.Parametr3.Contains("УП)") || a.DocumentsAllName.Contains("УП."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("производственную") || a.Parametr3.Contains("ПП ") || a.Parametr3.Contains("ПП)") || a.DocumentsAllName.Contains("ПП."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("выпускных") || a.Parametr3.Contains("ВКР ") || a.Parametr3.Contains("ВКР)") || a.DocumentsAllName.Contains("ВКР."))) ||
               ((a.DocumentType == 15) && (a.Parametr3!.Contains("курсовой") || a.Parametr3.Contains("курсовых") || a.Parametr3.Contains("курсового") || a.Parametr3.Contains("КП ") || a.Parametr3.Contains("КП)") || a.DocumentsAllName.Contains("КП.")))
               ))).Distinct().ToList();
                orderAll.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                var order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("курсовой") || a.Parametr3.Contains("курсовых") || a.Parametr3.Contains("курсового") || a.Parametr3.Contains("КП ") || a.Parametr3.Contains("КП)") || a.DocumentsAllName.Contains("КП.")))).Distinct().ToList();

                string ordersInfo = "";
                foreach (var ord in order)
                {

                    lastIndex = ord.DocumentsAllName.LastIndexOf(".");
                    if (int.Parse(ord.Parametr1!.Substring(3, 2)) >= 8)
                    {
                        int yearYch = int.Parse(ord.Parametr1.Substring(6, 4)) + 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году являлся руководителем или консультантом по выполнению курсовых проектирований студентов. " + " (3." + selection.ToString() + ".1. " + ord.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    else if (int.Parse(ord.Parametr1.Substring(3, 2)) < 8)
                    {
                        int yearYch = int.Parse(ord.Parametr1.Substring(6, 4)) - 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord.Parametr1.Substring(6, 4) + " учебном году являлся руководителем или консультантом по выполнению курсовых проектирований студентов. " + " (3." + selection.ToString() + ".1. " + ord.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    selection++;


                }
                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("выпускных") || a.Parametr3.Contains("ВКР ") || a.Parametr3.Contains("ВКР)") || a.DocumentsAllName.Contains("ВКР.")))).Distinct().ToList();

                foreach (var ord2 in order)
                {

                    lastIndex = ord2.DocumentsAllName.LastIndexOf(".");
                    if (int.Parse(ord2.Parametr1!.Substring(3, 2)) >= 8)
                    {
                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) + 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году являлся руководителем или консультантом по выполнению выпускных квалификационных работы обучающихся. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    else if (int.Parse(ord2.Parametr1.Substring(3, 2)) < 8)
                    {
                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) - 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord2.Parametr1.Substring(6, 4) + " учебном году являлся руководителем или консультантом по выполнению выпускных квалификационных работы обучающихся. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    selection++;


                }

                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("производственную") || a.Parametr3.Contains("ПП ") || a.Parametr3.Contains("ПП)") || a.DocumentsAllName.Contains("ПП.")))).Distinct().ToList();

                foreach (var ord2 in order)
                {

                    lastIndex = ord2.DocumentsAllName.LastIndexOf(".");
                    if (int.Parse(ord2.Parametr1!.Substring(3, 2)) >= 8)
                    {
                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) + 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году осуществлял руководство студентами в рамках производственной практики. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    else if (int.Parse(ord2.Parametr1.Substring(3, 2)) < 8)
                    {
                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) - 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord2.Parametr1.Substring(6, 4) + " учебном году осуществлял руководство студентами в рамках производственной практики. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    selection++;


                }


                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("учебные") || a.Parametr3.Contains("учебную") || a.Parametr3.Contains("УП ") || a.Parametr3.Contains("УП)") || a.DocumentsAllName.Contains("УП.")))).Distinct().ToList();

                foreach (var ord2 in order)
                {

                    lastIndex = ord2.DocumentsAllName.LastIndexOf(".");
                    if (int.Parse(ord2.Parametr1!.Substring(3, 2)) >= 8)
                    {
                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) + 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году осуществлял руководство студентами в рамках учебной практики. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    else if (int.Parse(ord2.Parametr1.Substring(3, 2)) < 8)
                    {
                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) - 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord2.Parametr1.Substring(6, 4) + " учебном году осуществлял руководство студентами в рамках учебной практики. " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    selection++;


                }

                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("по подготовке") && a.Parametr3.Contains("олимпиаде")))).Distinct().ToList();

                foreach (var ord2 in order)
                {

                    lastIndex = ord2.DocumentsAllName.LastIndexOf(".");
                    int index = ord2.DocumentsAllName.LastIndexOf("по подготовке") + 13;
                    if (int.Parse(ord2.Parametr1!.Substring(3, 2)) >= 8)
                    {
                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) + 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + "/" + yearYch.ToString() + " учебном году являлся руководителем по подготовке" + ord2.DocumentsAllName.Substring(index, lastIndex - index) + " . " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    else if (int.Parse(ord2.Parametr1.Substring(3, 2)) < 8)
                    {
                        int yearYch = int.Parse(ord2.Parametr1.Substring(6, 4)) - 1;
                        ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + yearYch.ToString() + "/" + ord2.Parametr1.Substring(6, 4) + " учебном году являлся руководителем по подготовке" + ord2.DocumentsAllName.Substring(index, lastIndex - index) + " . " + " (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    }
                    selection++;


                }
                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 10) && (a.DocumentUnique == 1) && (a.Parametr2 != "Участник"))).Distinct().ToList();
                var student = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 9) && (a.DocumentUnique == 1))).Distinct().ToList();
                student.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                int studNumber = 2;
                foreach (var ord2 in order)
                {

                    lastIndex = ord2.DocumentsAllName.LastIndexOf(".");

                    int yearYch = int.Parse(ord2.Parametr1!.Substring(6, 4));
                    ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + " году принимал участие чемпионате по стандартам WorldSkills Russia в роли «" + ord2.Parametr2 + "» по компетенции «" + ord2.Parametr5 + "». (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    foreach (var stud in student)
                    {
                        if (int.Parse(ord2.Parametr1.Substring(6, 4)) == int.Parse(stud.Parametr1!.Substring(6, 4)))
                        {
                            if (ord2.Parametr5 == stud.Parametr3)
                            {
                                ordersInfo = ordersInfo + "          - Участник " + stud.Parametr4 + " под руководством принимал участие в чемпионате по стандартам WorldSkills Russia в данной компетенции.";
                                if (stud.Parametr5 != null)
                                {
                                    ordersInfo = ordersInfo + " Место занятое студентом по результатам чемпионата: " + stud.Parametr5 + ". ";
                                }
                                lastIndex = stud.DocumentsAllName.LastIndexOf(".");
                                ordersInfo = ordersInfo + " (3." + selection.ToString() + "." + studNumber.ToString() + ". " + stud.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                                studNumber++;
                            }
                        }

                    }
                    selection++;
                    studNumber = 2;


                }
                order = orderAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 41) && (a.DocumentUnique == 1) && (a.Parametr2 != "Участник"))).Distinct().ToList();
                student = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 40) && (a.DocumentUnique == 1))).Distinct().ToList();

                student.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                studNumber = 2;
                foreach (var ord2 in order)
                {

                    lastIndex = ord2.DocumentsAllName.LastIndexOf(".");

                    int yearYch = int.Parse(ord2.Parametr1!.Substring(6, 4));
                    ordersInfo = ordersInfo + "         " + selection.ToString() + ". В " + ord2.Parametr1.Substring(6, 4) + " году принимал участие чемпионате DigitalSkills в роли «" + ord2.Parametr2 + "» по компетенции «" + ord2.Parametr5 + "». (3." + selection.ToString() + ".1. " + ord2.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    foreach (var stud in student)
                    {
                        if (int.Parse(ord2.Parametr1.Substring(6, 4)) == int.Parse(stud.Parametr1!.Substring(6, 4)))
                        {
                            if (ord2.Parametr5 == stud.Parametr3)
                            {
                                ordersInfo = ordersInfo + "          - Участник " + stud.Parametr4 + " под руководством принимал участие в чемпионате DigitalSkills в данной компетенции.";
                                if (stud.Parametr5 != null)
                                {
                                    ordersInfo = ordersInfo + " Место занятое студентом по результатам чемпионата: " + stud.Parametr5 + ". ";
                                }
                                lastIndex = stud.DocumentsAllName.LastIndexOf(".");
                                ordersInfo = ordersInfo + " (3." + selection.ToString() + "." + studNumber.ToString() + ". " + stud.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                                studNumber++;
                            }
                        }

                    }
                    selection++;
                    studNumber = 2;


                }
                selection = 1;
                //Перебор данных для заполнения Критерия IV. В данный критерий входят участия преподавателя в различных мероприятиях по типу Вебинар, Семинар..., а также организация дней открытых дверей,
                //также входят участие преподавателя в составах комиссий, ЦМК, ГЭК, а также участие в разработке УМК
                var individualAll = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
              ((a.DocumentType == 24) ||
              (a.DocumentType == 17) ||
              (a.DocumentType == 2) ||
              (a.DocumentType == 3) ||
              (a.DocumentType == 1) ||
              (a.DocumentType == 12) ||
              (a.DocumentType == 13) ||
              (a.DocumentType == 22) ||
              (a.DocumentType == 23) ||
              ((a.DocumentType == 15) && (a.Parametr3!.Contains("день открытых дверей") || a.Parametr3.Contains("дня открытых дверей") || a.Parametr3.Contains("День открытых дверей"))) ||
              (a.DocumentType == 4) ||
              (a.DocumentType == 5) ||
              (a.DocumentType == 21) ||
              (a.DocumentType == 20) ||
              (a.DocumentType == 19) ||
              (a.DocumentType == 18)
              ))).Distinct().ToList();
                individualAll.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                string individualParametr = "";
                var individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 24) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    individualParametr = individualParametr + "          " + selection.ToString() + ". В " + individ.Parametr1!.Substring(6, 4) + " принимал участие в работе круглого стола, организованного " + individ.Parametr2 + " на тему: «" + individ.Parametr3 + "». (4." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    selection++;

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 17) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    individualParametr = individualParametr + "          " + selection.ToString() + ". В " + individ.Parametr1!.Substring(6, 4) + " принимал участие в конференции, организованной " + individ.Parametr2 + " на тему: «" + individ.Parametr3 + "». (4." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    selection++;

                }
                string skill = "";
                int skillCount = 1;
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && ((a.DocumentType == 2) && (a.DocumentType == 3)) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    if (individ.DocumentType == 3)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Диплом о высшем образовании по специальности «" + individ.Parametr3 + "» в «" + individ.Parametr2 + "» с присвоением квалификации «" + individ.Parametr4 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }
                    if (individ.DocumentType == 2)
                    {
                        lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                        skill = skill + "          - Диплом о среднем профессиональном образовании по специальности «" + individ.Parametr3 + "» в «" + individ.Parametr2 + "» с присвоением квалификации «" + individ.Parametr4 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                        skillCount++;
                    }
                }

                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 1) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Диплом о начальном профессиональном образовании в «" + individ.Parametr2 + "» с присвоением квалификации «" + individ.Parametr3 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 12) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Диплом о профессиональной переподготовке по программе «" + individ.Parametr3 + "» в «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 13) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Удостоверение о повышении квалификации по программе «" + individ.Parametr3 + "» в «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 22) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Сертификат участника вебинара на тему  «" + individ.Parametr3 + "» организованного «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 23) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Сертификат участника семинара на тему  «" + individ.Parametr3 + "» организованного «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 18) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Сертификат участника мастер-класса на тему  «" + individ.Parametr3 + "» организованного «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;

                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 19) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Сертификат участника форума на тему  «" + individ.Parametr3 + "» организованного «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 20) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Сертификат участника панельной дискуссии на тему  «" + individ.Parametr3 + "» организованной «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 21) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Сертификат участника онлайн-класса на тему  «" + individ.Parametr3 + "» организованной «" + individ.Parametr2 + "». Дата выдачи: " + individ.Parametr1!.Substring(0, 3).Replace(".", " ") + Month[Int32.Parse(individ.Parametr1.Substring(3, 2))] + individ.Parametr1.Substring(5, 5).Replace(".", " ") + ". (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 32) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Стажировка по программе  «" + individ.Parametr3 + "» на базе «" + individ.Parametr2 + "». (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 5) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Сертификационный экзамен по теме  «" + individ.Parametr3 + "» от «" + individ.Parametr2 + "». (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 4) && (a.DocumentUnique == 1))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    skill = skill + "          - Вендорный сертификационный экзамен по теме  «" + individ.Parametr3 + "» от «" + individ.Parametr2 + "». (4." + selection.ToString() + "." + skillCount.ToString() + ". " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    skillCount++;


                }
                if (skill != "")
                {

                    individualParametr = individualParametr + "          " + selection.ToString() + ". За межаттестационный период постоянно повышал свой профессиональный опыт, получил следующие дипломы, удостоверения и сертификаты: \r\n" + skill;
                    selection++;
                }
                individual = individualAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("открытых дверей")))).ToList();
                individual.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    individualParametr = individualParametr + "          " + selection.ToString() + ". В " + individ.Parametr1!.Substring(6, 4) + " году принимал участие в проведении профориентационного мероприятия «День открытых дверей» в РЭУ им. Г.В. Плеханова. (4." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    selection++;


                }
                var activeAll = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
              ((a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("утверждении состава") || a.Parametr3.Contains("составе") || a.Parametr3.Contains("составах") || a.Parametr3.Contains("комиссиях")) ||
              (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ЦМК") || a.Parametr3.Contains("циклов")) ||
              (a.DocumentType == 16) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ГЭК") || a.Parametr3.Contains("государственных экзаменационных") || a.Parametr3.Contains("государственной экзаменационной")) ||
              (a.DocumentType == 33)
              ))).Distinct().ToList();
                activeAll.Sort((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName));
                string active = "";
                individual = activeAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("утверждении состава") || a.Parametr3.Contains("составе") || a.Parametr3.Contains("составах") || a.Parametr3.Contains("комиссиях")))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    int index = 0;
                    if (individ.Parametr3!.Contains("утверждении состава"))
                    {
                        index = individ.DocumentsAllName.LastIndexOf("утверждении состава") + 19;
                    }
                    else if (individ.Parametr3.Contains("составе"))
                    {
                        index = individ.DocumentsAllName.LastIndexOf("составе") + 7;
                    }
                    else if (individ.Parametr3.Contains("составах"))
                    {
                        index = individ.DocumentsAllName.LastIndexOf("составах") + 8;
                    }
                    else if (individ.Parametr3.Contains("комиссиях"))
                    {
                        index = individ.DocumentsAllName.LastIndexOf("комиссиях") + 9;
                    }
                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    active = active + "          " + selection.ToString() + ". В " + individ.Parametr1!.Substring(6, 4) + " году членом состава" + individ.DocumentsAllName.Substring(index, lastIndex - index) + " Московского приборостроительного техникума. (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    selection++;


                }
                individual = activeAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ЦМК") || a.Parametr3.Contains("циклов")))).Distinct().ToList();

                foreach (var individ in individual)
                {


                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    active = active + "          " + selection.ToString() + ". В " + individ.Parametr1!.Substring(6, 4) + " году членом состава цикловой методической комиссии Московского приборостроительного техникума. (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    selection++;


                }
                individual = activeAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 16) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ГЭК") || a.Parametr3.Contains("государственных экзаменационных") || a.Parametr3.Contains("государственной экзаменационной")))).Distinct().ToList();

                foreach (var individ in individual)
                {

                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    active = active + "          " + selection.ToString() + ". В " + individ.Parametr1!.Substring(6, 4) + " году членом государственной экзаменационной комиссии для проведения итоговой аттестации выпускников. (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";
                    selection++;


                }
                individual = activeAll.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 33) && (a.DocumentUnique == 1))).Distinct().ToList();

                if (individual.Count != 0)
                {
                    active = active + "          " + selection.ToString() + ". Принимал участие в разработке УМК по общепрофессиональным дисциплинам и профессиональным модулям: \r\n";
                }
                //РП / СР / КОС / КИМ / МУ / ПР / ЛР
                foreach (var individ in individual)
                {


                    lastIndex = individ.DocumentsAllName.LastIndexOf(".");
                    if (individ.Parametr3 == "РП")
                    {
                        active = active + "          - В " + individ.Parametr1!.Substring(6, 4) + " году рабочая программа для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                    }
                    if (individ.Parametr3 == "СР")
                    {
                        active = active + "          - В " + individ.Parametr1!.Substring(6, 4) + " году самостоятельные работы для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                    }
                    if (individ.Parametr3 == "КОС")
                    {
                        active = active + "          - В " + individ.Parametr1!.Substring(6, 4) + " году комплект оценочных средств для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                    }
                    if (individ.Parametr3 == "КИМ")
                    {
                        active = active + "          - В " + individ.Parametr1!.Substring(6, 4) + " году контрольно-измерительный материал для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                    }
                    if (individ.Parametr3 == "МУ")
                    {
                        active = active + "          - В " + individ.Parametr1!.Substring(6, 4) + " году методические указания для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                    }
                    if (individ.Parametr3 == "ПР")
                    {
                        active = active + "          - В " + individ.Parametr1!.Substring(6, 4) + " году практические работы для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                    }
                    if (individ.Parametr3 == "ЛР")
                    {
                        active = active + "          - В " + individ.Parametr1!.Substring(6, 4) + " году лабораторные работы для студентов специальности " + individ.Parametr4 + " по " + individ.Parametr5 + ", дисциплине «" + individ.Parametr6 + "». (5." + selection.ToString() + ".1. " + individ.DocumentsAllName.Substring(0, lastIndex) + ");\r\n";

                    }
                    selection++;


                }
                if (active != "")
                {
                    individualParametr = individualParametr + "          За межаттестационный период принимал активное участие в работе методических объединений и являлся: \r\n" + active;
                }
                //Сбор сокращенного ФИО пользователя
                string[] FIO = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio.Split(' ');
                string usersFIO = "";
                if (FIO.Length != 2)
                {
                    usersFIO = FIO[1].Substring(0, 1) + "." + FIO[2].Substring(0, 1) + ". " + FIO[0];
                }
                else
                {
                    usersFIO = FIO[1].Substring(0, 1) + ". " + FIO[0];
                }
                //Составление документа путем вставления собранных данных в созданный ранее шаблон DocX
#pragma warning disable CS0618 // Тип или член устарел
                letter.ReplaceText("%Parameter1%", db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio);
                letter.ReplaceText("%Parameter2%", mdkstr);
                letter.ReplaceText("%Parameter3%", documents[numbermin].Parametr5);
                letter.ReplaceText("%Parameter4%", documents[numbermax].Parametr5);
                letter.ReplaceText("%Parameter5%", documents[numbermin].Parametr6);
                letter.ReplaceText("%Parameter6%", documents[numbermax].Parametr6);
                letter.ReplaceText("%Parameter7%", ved2);
                letter.ReplaceText("%Parameter8%", ved);
                letter.ReplaceText("%Parameter9%", dist);
                letter.ReplaceText("%Parameter10%", statistic + "\r\n" + statistic2);
                letter.ReplaceText("%Parameter11%", ordersInfo);
                letter.ReplaceText("%Parameter12%", individualParametr);
                letter.ReplaceText("%Parameter13%", usersFIO);
#pragma warning restore CS0618 // Тип или член устарел
                letter.SaveAs(outputFileName);

                //Загрузка данных на Яндекс диск с учетом уникальности файла. В названии файла также присутствует ФИО пользователя системы
                string FolderUser = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio;
                string Name = "Информационная справка " + usersFIO + " (Первая категория) ";
                int i1 = 1;


                string Altername = Name;
                string extension = "docx";
                string oauthToken = _configuration.GetConnectionString("TokenAuth")!;
                var api = new DiskHttpApi(oauthToken);
                IDiskApi diskApi = new DiskHttpApi(oauthToken);



                var usersdoc = db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentsAllName.Contains(Name + "." + extension) && model.DocumentUnique == 1 && model.DocumentType == 100).ToListAsync();
                if (usersdoc.Result.Count == 0)
                {


                    DocumentsAll newDocument = new DocumentsAll
                    {
                        DocumentsAllName = Name + "." + extension,
                        DocumentType = 100,
                        UsersDocId = int.Parse(HttpContext.Session.GetString("Id_User")!),
                        DocumentUnique = 1,
                    };
                    db.DocumentsAlls.Add(newDocument);
                    db.SaveChanges();


                }
                else
                {
                    var contains = db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentUnique == 0 && model.DocumentType == 100).Distinct().ToList();

                    while (true)
                    {
                        Altername = Name + "(" + i1 + ")";
                        var usersdoc2 = contains.Where(model => model.DocumentsAllName.Contains(Altername + "." + extension)).Distinct().ToList();

                        if (usersdoc2.Count == 0)
                        {

                            DocumentsAll Doc6 = new DocumentsAll { };

                            Doc6 = new DocumentsAll
                            {

                                DocumentsAllName = Altername + "." + extension,
                                DocumentType = 100,
                                UsersDocId = int.Parse(HttpContext.Session.GetString("Id_User")!),
                                DocumentUnique = 0



                            };
                            db.DocumentsAlls.Add(Doc6);
                            db.SaveChanges();
                            break;
                        }

                        i1++;
                    }


                }


                if (i1 != 0)
                {
                }

                db.SaveChanges();

                await diskApi.Files.UploadFileAsync(path: "портфолио преподавателей/" + FolderUser + "/" + Altername + ".txt",

                                                    overwrite: false,
                                                    localFile: Directory.GetCurrentDirectory() + "/wwwroot/file/" + surname + ".templates3.docx",

                                                    cancellationToken: CancellationToken.None);




                var request = new MoveFileRequest
                {
                    From = "портфолио преподавателей/" + FolderUser + "/" + Altername + ".txt",
                    Path = "портфолио преподавателей/" + FolderUser + "/" + Altername + "." + extension,
                    Overwrite = false
                };
                await diskApi.Commands.MoveAndWaitAsync(
                    request: request,
                    cancellationToken: CancellationToken.None,
                    pullPeriod: null
                   );
                string pathDelete = (Directory.GetCurrentDirectory() + "/wwwroot/file/" + surname + ".templates3.docx").ToString();
                System.IO.File.Delete(pathDelete);


                return true;
            }
            catch { return false; }

        }
        /// <summary>
        /// Данный метод служит для проверки того, какие файлы необходимы пользователю для формирования информационной справки на первую категорию преподавателя
        /// метод собирает данные о наличии и выводит сообщение пользователю в виде всплывающего сообщения на экране.
        /// </summary>
        /// <returns>Сообщение о недостающих файлах для генерации</returns>
        public string InfoCheckerFirst()
        {
            string message = "";



            var documents = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 31) && (a.DocumentUnique == 1))).Distinct().ToList();

            if (documents.Count == 0)
            {
                message = message + " отсутствуют сводные ведомости;";
            }
            else { }

            var sertification = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentType == 7 || a.DocumentType == 8 || a.DocumentType == 9 || a.DocumentType == 40) && (a.DocumentUnique == 1))).Distinct().ToList();

            if (sertification.Count == 0)
            {
                message = message + " отсутствуют сертификации студентов и/или внеучебные достижения студентов;";
            }
            else { }

            var order = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
           (((a.DocumentType == 41) && (a.Parametr2 != "Участник")) ||
           ((a.DocumentType == 10) && (a.Parametr2 != "Участник")) ||
           ((a.DocumentType == 15) && (a.Parametr3!.Contains("по подготовке") && a.Parametr3.Contains("олимпиаде"))) ||
           ((a.DocumentType == 15) && (a.Parametr3!.Contains("учебные") || a.Parametr3.Contains("учебную") || a.Parametr3.Contains("УП ") || a.Parametr3.Contains("УП)") || a.DocumentsAllName.Contains("УП."))) ||
           ((a.DocumentType == 15) && (a.Parametr3!.Contains("производственную") || a.Parametr3.Contains("ПП ") || a.Parametr3.Contains("ПП)") || a.DocumentsAllName.Contains("ПП."))) ||
           ((a.DocumentType == 15) && (a.Parametr3!.Contains("выпускных") || a.Parametr3.Contains("ВКР ") || a.Parametr3.Contains("ВКР)") || a.DocumentsAllName.Contains("ВКР."))) ||
           ((a.DocumentType == 15) && (a.Parametr3!.Contains("курсовой") || a.Parametr3.Contains("курсовых") || a.Parametr3.Contains("курсового") || a.Parametr3.Contains("КП ") || a.Parametr3.Contains("КП)") || a.DocumentsAllName.Contains("КП.")))
           ))).Distinct().ToList();


            if (order.Count == 0)
            {
                message = message + " отсутствуют данные о кураторстве студентов по УП/ПП/ВКР/КП, олимпиадам или чемпионатам;";
            }
            else { }


            var individual = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && (a.DocumentUnique == 1) &&
           ((a.DocumentType == 24) ||
           (a.DocumentType == 17) ||
           (a.DocumentType == 2) ||
           (a.DocumentType == 3) ||
           (a.DocumentType == 1) ||
           (a.DocumentType == 12) ||
           (a.DocumentType == 13) ||
           (a.DocumentType == 22) ||
           (a.DocumentType == 23) ||
           ((a.DocumentType == 15) && (a.Parametr3!.Contains("день открытых дверей") || a.Parametr3.Contains("дня открытых дверей") || a.Parametr3.Contains("День открытых дверей"))) ||
           (a.DocumentType == 4) ||
           (a.DocumentType == 5) ||
           (a.DocumentType == 21) ||
           (a.DocumentType == 20) ||
           (a.DocumentType == 19) ||
           (a.DocumentType == 18) ||
           (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("утверждении состава") || a.Parametr3.Contains("составе") || a.Parametr3.Contains("составах") || a.Parametr3.Contains("комиссиях")) ||
           (a.DocumentType == 15) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ЦМК") || a.Parametr3.Contains("циклов")) ||
           (a.DocumentType == 16) && (a.DocumentUnique == 1) && (a.Parametr3!.Contains("ГЭК") || a.Parametr3.Contains("государственных экзаменационных") || a.Parametr3.Contains("государственной экзаменационной")) ||
           (a.DocumentType == 33)
           ))).Distinct().ToList();


            if (individual.Count == 0)
            {
                message = message + " отсутствуют данные о получении ВПО/СПО/НПО/ПП/ПК/сертификациях, участии в Круглом столе/конференциях/вебинарах/семинарах,участии в комиссиях и/или разработке УМК;";
            }
            else { }


            return message;
        }
        /// <summary>
        /// Метод обработки запроса по формированию информационной справки на получение первой квалификационной категории
        /// </summary>
        /// <returns>Статус формирования справки</returns>

        public async Task<string> NewDocumentCreateFirst()
        {
            //DiskCheckerAsync проверяет актуальность данных
            await DiskCheckerAsync(int.Parse(HttpContext.Session.GetString("Id_User")!));
            string result = InfoCheckerFirst();
            string message = "";

            if (result == "")
            {
                bool results = await CreateInformFirstAsync();

                if (results == true)
                {
                    message = "Информационная справка успешно сформирована";
                }
                if (results == false)
                {
                    message = "Ошибка формирования документа";
                }
            }
            if (result != "")
            {
                message = "Необходимо добавить в портфолио за межаттестационный период:" + result;

            }
            return message;
        }
        /// <summary>
        /// Метод отвечающий за создание индивидуального плана работы преподавателя (ИПРП) на основе шаблона данных.Часть данных вводится пользователем, а часть выгружаетя из портфолио.
        /// </summary>
        /// <param name="Parametr1">Учебный год</param>
        /// <param name="Parametr2">Название цикловой методической комиссии</param>
        /// <param name="Parametr4">Дисциплинa, МДК(перечислить)</param>
        /// <param name="Parametr5">Специальность</param>
        /// <param name="Parametr6">Курс(ы)</param>
        /// <param name="Parametr9">Занимаемые должности</param>
        /// <param name="Parametr13">Преподаваемые дисциплины</param>
        /// <param name="Parametr18">Количество учебных часов в первом семестре</param>
        /// <param name="Parametr20">Количество учебных часов в год</param>
        /// <param name="Parametr21">Работа по оснащению специализированных кабинетов</param>
        /// <param name="Parametr22">Подготовка УМК (в т.ч. изданные в течение отчетного периода КТП, рабочая программа, ФОС, методички), соответствие образовательным программам, предложение и перспективный план заданий.</param>
        /// <param name="Parametr23">Внедрение эффективных методов преподавания, обобщение, обмен педагогическим опытом, использование инновационных технологий, взаимопосещения занятий коллег.</param>
        /// <param name="Parametr24">Междисциплинарные связи</param>
        /// <param name="Parametr25">Организация самостоятельной работы обучающихся в т.ч. выполнения и проверка домашних заданий</param>
        /// <param name="Parametr26">Проведение открытых мероприятий (темы докладов на заседаниях ЦМК, открытых уроков и мероприятий, уроков-экскурсий, прочее).Работа по оснащению специализированных кабинетов</param>
        /// <param name="Parametr27">Проведение внеаудиторных мероприятий с обучающимися</param>
        /// <param name="Parametr28">Обеспечение дисциплин учебной литературой</param>
        /// <param name="Parametr29">Внедрения новых форм и методов обучения, средств активизации познавательной деятельности обучающихся</param>
        /// <param name="Parametr30">Подготовка программных материалов для промежуточных аттестаций и иных установленных форм контроля (анализ контрольных материалов и сессий)</param>
        /// <param name="Parametr31">Предложения по совершенствованию учебного процесса на перспективу</param>
        /// <param name="Parametr32">Организация самостоятельной и научно-исследовательской деятельности обучающихся (указать мероприятия, например название и место проведение олимпиады)</param>
        /// <param name="Parametr33">Статьи и публикации в учебном году</param>
        /// <param name="Parametr34">Прочие планируемые достижения в учебном году</param>
        /// <param name="Parametr35">Повышение квалификации(планирующиеся)</param>
        /// <param name="Parametr36">Стажировка, аспирантура, курсы, прочее(планирующиеся)</param>
        /// <returns>Сообщение с статусом формирования файла</returns>
        [HttpPost]
        public async Task<string> DownloadWord(string Parametr1, string Parametr2, string Parametr4, string Parametr5, string Parametr6, string Parametr9, string Parametr13, double Parametr18, double Parametr20, string Parametr21, string Parametr22, string Parametr23, string Parametr24, string Parametr25, string Parametr26, string Parametr27, string Parametr28, string Parametr29, string Parametr30, string Parametr31, string Parametr32, string Parametr33, string Parametr34, string Parametr35, string Parametr36)
        {
            try
            {
                ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();
                //DiskCheckerAsync проверяет актуальность данных
                await DiskCheckerAsync(int.Parse(HttpContext.Session.GetString("Id_User")!));
                try
                {
                    //Если опциональные параметры не заполнены сотрудником, автоматически проставляется надпись "Не планируется"
                    string Parametr8 = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoDateBirthday.ToString("dd MM yyyy");
                    if (Parametr21 == null) { Parametr21 = "Не планируется"; }
                    if (Parametr22 == null) { Parametr22 = "Не планируется"; }
                    if (Parametr23 == null) { Parametr23 = "Не планируется"; }
                    if (Parametr24 == null) { Parametr24 = "Не планируется"; }
                    if (Parametr25 == null) { Parametr25 = "Не планируется"; }
                    if (Parametr26 == null) { Parametr26 = "Не планируется"; }
                    if (Parametr27 == null) { Parametr27 = "Не планируется"; }
                    if (Parametr28 == null) { Parametr28 = "Не планируется"; }
                    if (Parametr29 == null) { Parametr29 = "Не планируется"; }
                    if (Parametr30 == null) { Parametr30 = "Не планируется"; }
                    if (Parametr31 == null) { Parametr31 = "Не планируется"; }
                    if (Parametr32 == null) { Parametr32 = "Не планируется"; }
                    if (Parametr33 == null) { Parametr33 = "Не планируется"; }
                    if (Parametr34 == null) { Parametr34 = "Не планируется"; }
                    if (Parametr35 == null) { Parametr35 = "Не планируется"; }
                    if (Parametr36 == null) { Parametr36 = "Не планируется"; }
                    //В название при создании добавляется фамилия пользователя. Из-за специфики сервера (разрешено только добавление файлов с англоязычными названиями), фамилия
                    //пользователя проходит транслитерацию на английский язык
                    TranslitMethods.Translitter trn = new TranslitMethods.Translitter();

                    surname = trn.Translit(db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoLastName, TranslitMethods.TranslitType.Gost).ToString();
                    string fileNameTemplate = Directory.GetCurrentDirectory() + "/wwwroot/file/{0}.templates1.docx";
                    string outputFileName = string.Format(fileNameTemplate, surname);
                    string obraz = "";
                    string spec = "";
                    int i = 0;

                    //Перебор данных портфолио на определение необходимых для занесения в ИПРП данных
                    var documents = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && ((a.DocumentType == 1) || (a.DocumentType == 2) || (a.DocumentType == 3) || ((a.DocumentType == 12) && (a.Parametr4 != null))) && (a.DocumentUnique == 1))).Distinct().ToList();
                    documents.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                    foreach (var doc in documents)
                    {
                        if (documents[i].DocumentType == 1)//НПО
                        {
                            obraz = obraz + (i + 1).ToString() + ". " + documents[i].Parametr2 + ", начальное профессиональное образование, " + (documents[i].Parametr1!).Substring(6) + "\r\n";
                            spec = spec + (i + 1).ToString() + ". " + documents[i].Parametr3 + "\r\n";
                        }
                        else if (documents[i].DocumentType == 2) //СПО
                        {
                            obraz = obraz + (i + 1).ToString() + ". " + documents[i].Parametr2 + ", среднее профессиональное образование, " + (documents[i].Parametr1!).Substring(6) + "\r\n";
                            spec = spec + (i + 1).ToString() + ". " + documents[i].Parametr3 + ", " + documents[i].Parametr4 + "\r\n";
                        }
                        else if (documents[i].DocumentType == 3)//ВЫШКА
                        {
                            obraz = obraz + (i + 1).ToString() + ". Высшее, " + documents[i].Parametr2 + ", " + (documents[i].Parametr1!).Substring(6) + "\r\n";
                            spec = spec + (i + 1).ToString() + ". " + documents[i].Parametr3 + ", " + documents[i].Parametr4 + "\r\n";
                        }
                        else if (documents[i].DocumentType == 12)//ПП
                        {
                            obraz = obraz + (i + 1).ToString() + ". " + documents[i].Parametr2 + ", профессиональная переподготовка, " + (documents[i].Parametr1!).Substring(6) + "\r\n";
                            spec = spec + (i + 1).ToString() + ". " + documents[i].Parametr3 + ", " + documents[i].Parametr4 + "\r\n";
                        }

                        i++;
                    }
                    string OldS = DateTime.Now.ToString("yyyyMM01");
                    DateTime r = DateTime.Parse(db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoDateStartWorkMpt.ToString());
                    string NewS = r.ToString("yyyyMM01");
                    DateTime r2 = DateTime.Parse(db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoDateStartWork.ToString());
                    string NewS2 = r2.ToString("yyyyMM01");
                    DateTime r3 = DateTime.Parse(db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoDateStartWorkTeacher.ToString());
                    string NewS3 = r3.ToString("yyyyMM01");
                    double year1 = (Double.Parse(OldS) - Double.Parse(NewS)) / 10000;
                    int YearWork = (int)year1;
                    double year2 = (Double.Parse(OldS) - Double.Parse(NewS2)) / 10000;
                    int YearWork1 = (int)year2;
                    double year3 = (Double.Parse(OldS) - Double.Parse(NewS3)) / 10000;
                    int YearWork2 = (int)year3;
                    int maxValue = 0;
                    string place = "";
                    var PP = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && ((a.DocumentType == 13)) && (a.DocumentUnique == 1))).Distinct().ToList();
                    PP.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                    int number = 0;
                    string PPStr = "";

                    foreach (var doc1 in PP)
                    {
                        string fir = PP[number].Parametr1!.Substring(6);
                        int Strdate1 = Int32.Parse(fir);
                        DateTime sec = DateTime.Now;
                        int Olddate1 = Int32.Parse(sec.ToString("yyyy"));
                        int gh1 = Olddate1 - Strdate1;
                        if (gh1 <= 5)
                        {
                            PPStr = PPStr + "- Повышение квалификации, " + PP[number].Parametr2 + ", " + PP[number].Parametr3 + ", " + (PP[number].Parametr1!).Substring(6) + "\r\n";
                        }
                        string gf = PP[number].Parametr1!.Replace(" ", "").Replace(".", "");
                        string StrdateNew = (gf.Substring(4)) + (gf.Substring(2, 3)) + gf[0] + (gf.Substring(1, 1));
                        int Strdate = Int32.Parse(StrdateNew);
                        string gf2 = PP[maxValue].Parametr1!.Replace(" ", "").Replace(".", "");
                        string OlddateNew = (gf2.Substring(4)) + (gf2.Substring(2, 3)) + gf2[0] + (gf2.Substring(1, 1));
                        int Olddate = Int32.Parse(OlddateNew);
                        int gh = Olddate - Strdate;
                        if (gh <= 0)
                        {
                            maxValue = number;
                        }

                        number++;
                    }
                    try
                    {
                        place = PP[maxValue].Parametr2 + ", " + PP[maxValue].Parametr3 + ", " + (PP[maxValue].Parametr1!).Substring(6);
                    }
                    catch { }

                    var Staj = db.DocumentsAlls.Where(a => (a.UsersDocId == int.Parse(HttpContext.Session.GetString("Id_User")!) && ((a.DocumentType == 32)) && (a.DocumentUnique == 1))).Distinct().ToList();
                    Staj.Sort(((a, b) => a.DocumentsAllName.CompareTo(b.DocumentsAllName)));
                    int numbers = 0;
                    string StajStr = "";
                    foreach (var doc2 in Staj)
                    {
                        string fir = Staj[numbers].Parametr1!.Substring(6);
                        int Strdate2 = Int32.Parse(fir);
                        DateTime sec = DateTime.Now;
                        int Olddate2 = Int32.Parse(sec.ToString("yyyy"));
                        int gh2 = Olddate2 - Strdate2;
                        if (gh2 <= 5)
                        {
                            StajStr = StajStr + "- Стажировка в " + Staj[numbers].Parametr2 + ", " + (Staj[numbers].Parametr1!).Substring(6) + "\r\n";
                        }

                        numbers++;
                    }
                    var kategory = db.Kategories.Find(Int32.Parse(HttpContext.Session.GetString("Id_User")!));

                    string kat = kategory!.KategoryType + ", " + kategory.KategoryDate?.ToString("dd.MM.yyyy");
                    string date = Parametr8.Substring(0, 3) + Month[Int32.Parse(Parametr8.Substring(3, 2))] + Parametr8.Substring(5, 5);

                    DocX letter = GetRejectionLetterTemplate();
                    if (obraz == "") { obraz = "Нет"; }
                    if (spec == "") { spec = "Нет"; }
                    if (kategory.KategoryType == null) { kat = "Нет"; }
                    if (place == "") { place = "Нет"; }
                    if (PPStr == "") { PPStr = "Нет"; }
                    if (StajStr == "") { StajStr = "Нет"; }
#pragma warning disable CS0618 // Тип или член устарел
                    //Составление документа путем вставления собранных данных в созданный ранее шаблон DocX
                    letter.ReplaceText("%Parameter1%", Parametr1);
                    letter.ReplaceText("%Parameter2%", Parametr2);
                    letter.ReplaceText("%Parameter3%", db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio.ToString());
                    letter.ReplaceText("%Parameter4%", Parametr4);
                    letter.ReplaceText("%Parameter5%", Parametr5);
                    letter.ReplaceText("%Parameter6%", Parametr6);
                    letter.ReplaceText("%Parameter7%", date);
                    letter.ReplaceText("%Parameter8%", Parametr9);
                    letter.ReplaceText("%Parameter9%", obraz);
                    letter.ReplaceText("%Parameter10%", spec);
                    letter.ReplaceText("%Parameter11%", YearWork.ToString() + " лет (с " + r.ToString("yyyy") + " года)");
                    letter.ReplaceText("%Parameter12%", Parametr13);
                    letter.ReplaceText("%Parameter13%", kat);
                    letter.ReplaceText("%Parameter14%", YearWork1.ToString());
                    letter.ReplaceText("%Parameter15%", YearWork2.ToString());
                    letter.ReplaceText("%Parameter16%", place);
                    letter.ReplaceText("%Parameter17%", "Нет");
                    letter.ReplaceText("%Parameter18%", Parametr18.ToString());
                    letter.ReplaceText("%Parameter19%", (Parametr20 - Parametr18).ToString());
                    letter.ReplaceText("%Parameter20%", Parametr20.ToString());
                    letter.ReplaceText("%Parameter21%", Parametr21);
                    letter.ReplaceText("%Parameter22%", Parametr22);
                    letter.ReplaceText("%Parameter23%", Parametr23);
                    letter.ReplaceText("%Parameter24%", Parametr24);
                    letter.ReplaceText("%Parameter25%", Parametr25);
                    letter.ReplaceText("%Parameter26%", Parametr26);
                    letter.ReplaceText("%Parameter27%", Parametr27);
                    letter.ReplaceText("%Parameter28%", Parametr28);
                    letter.ReplaceText("%Parameter29%", Parametr29);
                    letter.ReplaceText("%Parameter30%", Parametr30);
                    letter.ReplaceText("%Parameter31%", Parametr31);
                    letter.ReplaceText("%Parameter32%", Parametr32);
                    letter.ReplaceText("%Parameter33%", Parametr33);
                    letter.ReplaceText("%Parameter34%", Parametr34);
                    letter.ReplaceText("%Parameter35%", PPStr);
                    letter.ReplaceText("%Parameter36%", Parametr35);
                    letter.ReplaceText("%Parameter37%", StajStr);
                    letter.ReplaceText("%Parameter38%", Parametr36);
#pragma warning restore CS0618 // Тип или член устарел
                    letter.SaveAs(outputFileName);
                    string FolderUser = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio;
                    string User = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoLastName;
                    string Name = "ИПРП " + Parametr1.Replace("/", "-") + " " + FolderUser;
                    int i1 = 1;
                    //Загрузка данных на Яндекс диск с учетом уникальности файла. В названии файла также присутствует ФИО пользователя системы

                    string Altername = Name;
                    string extension = "docx";
                    string oauthToken = _configuration.GetConnectionString("TokenAuth")!;
                    var api = new DiskHttpApi(oauthToken);
                    IDiskApi diskApi = new DiskHttpApi(oauthToken);



                    var usersdoc = db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentsAllName == (Name + "." + extension)).ToListAsync();
                    if (usersdoc.Result.Count == 0)
                    {


                        DocumentsAll newDocument = new DocumentsAll
                        {
                            DocumentsAllName = Name + "." + extension,
                            DocumentType = 100,
                            UsersDocId = int.Parse(HttpContext.Session.GetString("Id_User")!),
                            DocumentUnique = 1,
                        };
                        db.DocumentsAlls.Add(newDocument);
                        db.SaveChanges();


                    }
                    else
                    {
                        while (true)
                        {
                            Altername = Name + "(" + i1 + ")";
                            var usersdoc2 = db.DocumentsAlls.Where(model => model.UsersDocId == Int32.Parse(HttpContext.Session.GetString("Id_User")!) && model.DocumentsAllName == (Altername + "." + extension)).ToListAsync();


                            if (usersdoc2.Result.Count == 0)
                            {

                                DocumentsAll Doc6 = new DocumentsAll { };

                                Doc6 = new DocumentsAll
                                {

                                    DocumentsAllName = Altername + "." + extension,
                                    DocumentType = 100,
                                    UsersDocId = int.Parse(HttpContext.Session.GetString("Id_User")!),
                                    DocumentUnique = 0



                                };
                                db.DocumentsAlls.Add(Doc6);
                                db.SaveChanges();
                                break;
                            }

                            i1++;
                        }


                    }


                    if (i1 != 0)
                    {
                    }

                    db.SaveChanges();

                    await diskApi.Files.UploadFileAsync(path: "портфолио преподавателей/" + FolderUser + "/" + Altername + ".txt",

                                                        overwrite: false,
                                                        localFile: Directory.GetCurrentDirectory() + "/wwwroot/file/" + surname + ".templates1.docx",

                                                        cancellationToken: CancellationToken.None);




                    var request = new MoveFileRequest
                    {
                        From = "портфолио преподавателей/" + FolderUser + "/" + Altername + ".txt",
                        Path = "портфолио преподавателей/" + FolderUser + "/" + Altername + "." + extension,
                        Overwrite = false
                    };
                    await diskApi.Commands.MoveAndWaitAsync(
                        request: request,
                        cancellationToken: CancellationToken.None,
                        pullPeriod: null
                       );
                    string pathDelete = (Directory.GetCurrentDirectory() + "/wwwroot/file/" + surname + ".templates1.docx").ToString();
                    System.IO.File.Delete(pathDelete);


                    return "План успешно сформирован";
                }
                catch
                {
                    return "Ошибка формирования файла";
                }
            }
            catch
            {
                return "Ошибка формирования файла";
            }
        }
        /// <summary>
        /// Метод отвечающий за получение шаблона Индивидуального плана работы преподавателя в формате DocX
        /// </summary>
        /// <returns>Шаблон ИПРП в формате DocX</returns>
        private DocX GetRejectionLetterTemplate()
        {
            string fileName = Directory.GetCurrentDirectory() + "/wwwroot/templates/templates1.docx";
            var doc = DocX.Load(fileName);
            return doc;
        }
        /// <summary>
        /// Метод отвечающий за открытие файлов пользователей системы. Для открытия происходит получение Публичной ссылки на файл
        /// </summary>
        /// <param name="IdFile">ID файла</param>
        /// <param name="Users">ID пользователя</param>
        /// <returns>Открытие ссылки в отдельным окне браузера</returns>
        [HttpPost]
        public async Task<IActionResult> OpenFile2Async(string IdFile, int? Users)
        {
            ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();

            string oauthToken = _configuration.GetConnectionString("TokenAuth")!;
            var api = new DiskHttpApi(oauthToken);
            IDiskApi diskApi = new DiskHttpApi(oauthToken);
            string FolderUser = "";
            if (Users != null)
            {
                FolderUser = db.UsersInfos.Where(a => a.UsersSId == Users).Single().UsersInfoFio;
            }
            else
            {
                FolderUser = db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().UsersInfoFio;
            }
            string path = "/портфолио преподавателей/" + FolderUser + "/" + IdFile;

            Link resultPP = await api.MetaInfo.PublishFolderAsync(path).ConfigureAwait(true);

            Resource fooResourceDescription = await api.MetaInfo.GetInfoAsync(new ResourceRequest { Path = path }, CancellationToken.None);

            System.Threading.Thread.Sleep(500);

            string pubS = fooResourceDescription.PublicUrl;
            Debug.Write(pubS);
            return Redirect(pubS);
        }
        /// <summary>
        /// Метод отвечающий за смену пароля пользователя
        /// </summary>
        /// <param name="IdUsers">ID пользователя</param>
        /// <param name="Password">Старый пароль</param>
        /// <param name="PasswordNew">Новый пароль</param>
        /// <returns>Статус изменения пароля</returns>
        [HttpPost]
        public async Task<string> ChangePassword(string IdUsers, string Password, string PasswordNew)
        {
            try
            {
                User userLogin = db.Users.FirstOrDefault(p => p.IdUsers == int.Parse(HttpContext.Session.GetString("Id_User")!))!;
                if (TrueHash(Password, userLogin!.UsersPassword) == true)
                {
                    var Update = db.Users.Find(Int32.Parse(HttpContext.Session.GetString("Id_User")!));
                    Update!.UsersPassword = Hash(PasswordNew)!;
                    db.Entry(Update).State = EntityState.Modified;
                    db.SaveChanges();
                    return "Пароль успешно сменен";
                }
                else
                {
                    return "Старый пароль не верный";
                }


            }
            catch { return "Ошибка смены пароля"; }
        }
        /// <summary>
        /// Метод отвечающий за смену категории у текущего авторизованного пользователя
        /// </summary>
        /// <param name="Kategor">Категория преподавателя</param>
        /// <param name="DateEnd">Дата окончания</param>
        /// <returns>Отображение страницы профиля после изменения категории</returns>
        [HttpPost]
        public async Task<IActionResult> ChangeKateg(string Kategor, DateTime DateEnd)
        {
            try
            {

                var Update = db.Kategories.Find(Int32.Parse(HttpContext.Session.GetString("Id_User")!));
                Update!.KategoryType = Kategor;
                Update.KategoryDate = DateEnd;
                db.Entry(Update).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index_Profile");
            }
            catch { return RedirectToAction("Index_MyDocument"); }
        }
        /// <summary>
        /// Метод удаления категории пользователя
        /// </summary>
        /// <returns>Страница пользователя</returns>
        [HttpPost]
        public async Task<IActionResult> DeleteKateg()
        {
            try
            {

                var Update = db.Kategories.Find(Int32.Parse(HttpContext.Session.GetString("Id_User")!));
                Update!.KategoryType = null;
                Update.KategoryDate = null;
                db.Entry(Update).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index_Profile");
            }
            catch { return RedirectToAction("Index_Profile"); }
        }
        /// <summary>
        /// Метод отвечающий за изменение роли пользователя на Администратор
        /// </summary>
        /// <param name="IdUser">ID пользователя</param>
        /// <returns>Представление отображения всех пользователей  после изменения роли на Администратор</returns>
        [HttpPost]
        public async Task<IActionResult> UserUp(string IdUser)
        {
            try
            {
                var Update = db.UsersInfos.Find(Int32.Parse(IdUser));
                Update!.RolesId = 1;
                db.Entry(Update).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index_AllUsers");
            }
            catch { return RedirectToAction("Index_AllUsers"); }
        }
        /// <summary>
        /// Метод отвечающий за изменение роли пользователя на Сотрудник
        /// </summary>
        /// <param name="IdUser">ID пользователя</param>
        /// <returns>Представление отображения всех пользователей  после изменения роли на Сотрудник</returns>
        [HttpPost]
        public async Task<IActionResult> UserDown(string IdUser)
        {
            try
            {

                var Update = db.UsersInfos.Find(Int32.Parse(IdUser));
                Update!.RolesId = 2;
                db.Entry(Update).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index_AllUsers");
            }
            catch { return RedirectToAction("Index_AllUsers"); }
        }
        /// <summary>
        /// Метод создания массива байтов для Хеширования пароля
        /// </summary>
        /// <param name="b1">Массив байтов</param>
        /// <param name="b2">Массив байтов</param>
        /// <returns>Статус сравнения в формате bool</returns>
        public static bool ByteArraysEqual(byte[] b1, byte[] b2)
        {
            if (b1 == b2) return true;
            if (b1 == null || b2 == null) return false;
            if (b1.Length != b2.Length) return false;
            for (int i = 0; i < b1.Length; i++)
            {
                if (b1[i] != b2[i]) return false;
            }
            return true;
        }
        /// <summary>
        /// Метод, хеширующий пароль пользователя с помощью библиотеки Rfc2898DeriveBytes, итерации - 1000
        /// </summary>
        /// <param name="Password">Пароль пользователя</param>
        /// <returns>Строка в формате Base64</returns>
        public string? Hash(string Password)
        {
            if (Password != null)
            {
                byte[] salt;
                byte[] res;
                using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(Password, 0x10, 0x3e8))
                {
                    salt = bytes.Salt;
                    res = bytes.GetBytes(0x20);
                }
                byte[] place = new byte[0x31];
                Buffer.BlockCopy(salt, 0, place, 1, 0x10);
                Buffer.BlockCopy(res, 0, place, 0x11, 0x20);
                return Convert.ToBase64String(place);
            }
            else return null;
        }
        /// <summary>
        /// Метод отвещающий за проверку наличия логина пользователя в системе
        /// </summary>
        /// <param name="Password">Пароля пользователя</param>
        /// <param name="Email">Логин (email) пользователя</param>
        /// <returns>Статус проверки пароля в формате bool</returns>
        public bool TryLogin(string Password, string Email)
        {
            if (Email != null || Password != null)
            {
                User userLogin = db.Users.FirstOrDefault(p => p.UsersEmail == Email)!;

                if (userLogin != null)
                {
                    if (TrueHash(Password, userLogin.UsersPassword) == true)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        /// <summary>
        /// Метод отвечающий за авторизацию пользователя в системе
        /// </summary>
        /// <param name="Password">Пароль пользователя</param>
        /// <param name="Email">Логин (email) пользователя</param>
        /// <returns>В зависимости от успешности выполнения команды - отображение станицы с документами либо отображение страницы авторизации</returns> 
        [HttpPost]
        public IActionResult Auth(string Password, string Email)
        {
            if (TryLogin(Password, Email))
            {
                User userLogin = db.Users.FirstOrDefault(p => p.UsersEmail == Email)!;
                HttpContext.Session.SetString("Id_User", userLogin.IdUsers.ToString());
                ViewBag.Role = (db.UsersInfos.Where(a => a.UsersSId == int.Parse(HttpContext.Session.GetString("Id_User")!)).Single().RolesId).ToString();
                return RedirectToAction("Index_Doc");
            }
            else
            {
                return RedirectToAction("Index_Auth");
            }

        }
        /// <summary>
        /// Метод отвечающий за сравнение хешированных паролей пользователя
        /// </summary>
        /// <param name="Password">Пароль введенный</param>
        /// <param name="PasswordHashed">Пароль хешированный</param>
        /// <returns>Статус стравнения двух паролей в формате bool</returns>
        public bool TrueHash(string Password, string PasswordHashed)
        {
            if (Password != null || PasswordHashed != null)
            {
                byte[] buffer;
                byte[] bytehash = Convert.FromBase64String(PasswordHashed);
                if (bytehash.Length != 0x31 || bytehash[0] != 0)
                { return false; }
                byte[] place = new byte[0x10];
                Buffer.BlockCopy(bytehash, 1, place, 0, 0x10);
                byte[] place2 = new byte[0x20];
                Buffer.BlockCopy(bytehash, 0x11, place2, 0, 0x20);
                using (Rfc2898DeriveBytes bytes = new Rfc2898DeriveBytes(Password!, place, 0x3e8))
                {
                    buffer = bytes.GetBytes(0x20);

                }

                return ByteArraysEqual(buffer, place2);
            }
            else return false;

        }
        /// <summary>
        /// Метод отвечающий за регистрацию пользователей в системе
        /// </summary>
        /// <param name="Name">Имя</param>
        /// <param name="Fam">Фамилия</param>
        /// <param name="Ot">Отчество</param>
        /// <param name="Birth">Дата рождения</param>
        /// <param name="DateStart">Дата начала работы</param>
        /// <param name="DateStartMPT">Дата начала работы в МПТ</param>
        /// <param name="DateStartTeach">Дата начала преподавательской деятельности</param>
        /// <param name="Email">Email</param>
        /// <param name="Password">Пароль</param>
        /// <returns>В зависимости от успешности выполнения - отображение страницы регистрации или авторизации</returns>
        [HttpPost]
        public async Task<string> Index_Register_Users(string Name, string Fam, string Ot, DateTime Birth, DateTime DateStart, DateTime DateStartMPT, DateTime DateStartTeach, string Email, string Password)
        {

            try
            {
                var userEmail = db.Users.Where(model => model.UsersEmail == Email).Distinct().ToList();
                var userFIO = db.UsersInfos.Where(model => model.UsersInfoFio == (Fam + " " + Name + " " + Ot)).Distinct().ToList();
                if (userEmail.Count == 0 && userFIO.Count == 0)
                {
                    User user = new User
                    {
                        UsersEmail = Email,
                        UsersPassword = Hash(Password)!,
                        UsersStatus = "Open",
                    };
                    db.Users.Add(user);
                    db.SaveChanges();

                    UsersInfo info = new UsersInfo
                    {
                        UsersInfoFio = Fam + " " + Name + " " + Ot,
                        UsersInfoLastName = Fam,
                        UsersInfoName = Name,
                        UsersInfoMiddleName = Ot,
                        UsersInfoDateBirthday = Birth,
                        UsersInfoDateStartWork = DateStart,
                        UsersInfoDateStartWorkMpt = DateStartMPT,
                        UsersInfoDateStartWorkTeacher = DateStartTeach,
                        UsersSId = (db.Users.Where(a => a.UsersEmail == Email).Single().IdUsers),
                        RolesId = 2

                    };
                    db.UsersInfos.Add(info);
                    await db.SaveChangesAsync();

                    string FIO = Fam + " " + Name + " " + Ot;

                    string oauthToken = _configuration.GetConnectionString("TokenAuth")!;
                    var api = new DiskHttpApi(oauthToken);
                    string folderName = "/" + FIO;
                    var usersdoc = db.UsersInfos.Where(model => model.UsersInfoFio == FIO).ToListAsync();

                    if (usersdoc.Result.Count == 1)
                    {
                        await api.Commands.CreateDictionaryAsync("/портфолио преподавателей" + folderName);

                    }
                    return "Пользователь успешно зарегистрирован";
                }
                else if (userEmail.Count != 0)
                {
                    return "Данный email уже существует в системе";
                }
                else
                {
                    return "Данный пользователь с этим ФИО уже существует в системе";
                }

            }
            catch { return "Ошибка регистрации"; }

        }
        /// <summary>
        /// Метод отвечающий за очистку сессии пользователя и выход из системы
        /// </summary>
        /// <returns>Отображение страницы авторизации</returns>
        [HttpGet]
        public async Task<IActionResult> Exit_Profile()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index_Auth");

        }


    }
}

//Дополнение для будущего обновления, связанного с возможностью корректного скачивания файлов напрямую из приложения
/*
<a download href="" referrerpolicy="no-referrer"></a> 
 
*/