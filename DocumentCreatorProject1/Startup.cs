﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using DocumentCreatorProject1.Models;

namespace DocumentCreatorProject1
{
    /// <summary>
    /// Настройка проекта
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Настройка доступа к конфигурации проекта
        /// </summary>
        /// <param name="configuration">Конфигурация</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Конфигурация проекта
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Настройка дополнительных сервисов в проекте
        /// </summary>
        /// <param name="services">Сервисы</param>
        public void ConfigureServices(IServiceCollection services)
        {
           
            string connection = Configuration.GetConnectionString("DatabaseConnection")! ;
            services.AddDbContext<MptDocCreateContext>(options => options.UseSqlServer(connection));
            services.AddControllersWithViews();
            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(60);
            });
        }
        /// <summary>
        /// Настройка конфигурации проекта
        /// </summary>
        /// <param name="app">Конвейер запросов и ответов приложения</param>
        /// <param name="env">Информация о текущей среде выполнения</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=index_auth}/{id?}");
            });
        }
    }
}
