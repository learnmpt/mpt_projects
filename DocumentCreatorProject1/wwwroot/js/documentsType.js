﻿
    (function ($) {
        function setChecked(target) {
            var checked = $(target).find("input[type='checkbox']:checked").length;
            if (checked) {
                $(target).find('select option:first').html('Выбрано: ' + checked);
            } else {
                $(target).find('select option:first').html('Выберите из списка');
            }
        }

        $.fn.checkselect = function () {
            this.wrapInner('<div class="checkselect-popup"></div>');
            this.prepend(
                '<div class="checkselect-control">' +
                '<select class="form-control"><option></option></select>' +
                '<div class="checkselect-over"></div>' +
                '</div>'
            );

            this.each(function () {
                setChecked(this);
            });
            this.find('input[type="checkbox"]').click(function () {
                setChecked($(this).parents('.checkselect'));
            });

            this.parent().find('.checkselect-control').on('click', function () {
                $popup = $(this).next();
                $('.checkselect-popup').not($popup).css('display', 'none');
                if ($popup.is(':hidden')) {
                    $popup.css('display', 'block');
                    $(this).find('select').focus();
                } else {
                    $popup.css('display', 'none');
                }
            });

            $('html, body').on('click', function (e) {
                if ($(e.target).closest('.checkselect').length == 0) {
                    $('.checkselect-popup').css('display', 'none');
                }
            });
        };
    })(jQuery);

    $('.checkselect').checkselect();


    const input = document.getElementById('input')

    input.addEventListener('change', (event) => {
        const target = event.target
        if (target.files && target.files[0]) {

            const maxAllowedSize = 30 * 1024 * 1024;
            if (target.files[0].size > maxAllowedSize) {
                target.value = ''
            }
        }
    })

    var doc = {
        '38': 'Мероприятие или название',
        '42': 'Предмет',
        '9': '',
        '10': '',
        '40': '',
        '41': '',
        '11': 'Название чемпионата',

    };
    var doc2 = {
        '1': 'Квалификация',
        '2': 'Специальность',
        '3': 'Специальность',
        '4': 'Название теста',
        '5': 'Название теста',
        '6': 'Название программы',
        '7': 'Уровень',
        '8': 'Название теста',
        '10': 'Регион',
         '41': 'Регион',
         '42': 'Группа',
        '12': 'Название программы',
        '13': 'Название программы',
        '14': 'Название программы',
        '15': '',
        '16': '',
        '17': 'Название мероприятия',
        '18': 'Название мероприятия',
        '19': 'Название мероприятия',
        '20': 'Название мероприятия',
        '21': 'Программы',
        '22': 'Название мероприятия',
        '23': 'Название мероприятия',
        '24': 'Название мероприятия',
        '25': 'О чём',
        '26': 'О чём',
        '27': 'Название статьи',
        '28': 'Название статьи',
        '29': 'Название статьи',
        '30': 'Название статьи',
        '31': 'Учебный год',
        '32': 'Название',
        '33': '',
        '34': 'Документ',
        '35': 'Заслуга',
        '36': 'Заслуга',
        '37': 'Название мероприятия',
    };
    var doc3 = {
        '2': 'Квалификация',
        '3': 'Квалификация',
        '7': 'Конкурс, номинация',
        '8': 'ФИО студента',
        '10': 'Компетенция',
        '41': 'Компетенция',
        '12': 'Квалификация(опционально)',
        '31': 'Предмет/МДК',
        '33': 'Код специальности',
        '34': 'Название'

    };
    var doc4 = {
        '7': 'ФИО студента',
        '9': 'Компетенция',
        '40': 'Компетенция',
        '31': 'Средний балл',
        '33': 'Код'

    };
    var doc5 = {
        '9': 'ФИО',
        '40': 'ФИО',
        '31': 'Качество успеваемости',
        '33': 'Предмет'

    };

    function updDoc() {
        var key = $("#ddlViewBy :selected").val();
        if (key == 10 || key == 41) {
            document.getElementById('divFirst').style.display = 'none';
            document.getElementById('divSecond').style.display = 'block';
            document.getElementById('divThird').style.display = 'block';
            document.getElementById("one").required = false;
        }
        else if (key == 9 || key == 40) {
            document.getElementById('divFirst').style.display = 'none';
            document.getElementById('divSecond').style.display = 'none';
            document.getElementById('divThird').style.display = 'block';
            document.getElementById("one").required = false;
        }
        else if (doc[key] != null) {
            document.getElementById('divFirst').style.display = 'block';
            document.getElementById('divSecond').style.display = 'none';
            document.getElementById('divThird').style.display = 'none';
            document.getElementById("one").required = true;

            $("#textNew").text(doc[key]);
        }
        else {
            document.getElementById('divFirst').style.display = 'block';
            document.getElementById('divSecond').style.display = 'none';
            document.getElementById('divThird').style.display = 'none';
            document.getElementById("one").required = true;
            $("#textNew").text('Организация');

        }


        if (doc2[key] == null) {
            document.getElementById('div4').style.display = 'none';
            document.getElementById('div5').style.display = 'none';
            document.getElementById('div6').style.display = 'none';
            document.getElementById("two").required = false;
            document.getElementById("three").required = false;
            document.getElementById("nine").required = false;
            document.getElementById("four").required = false;
        }
        else if (key == 15 || key == 16) {
            document.getElementById('div4').style.display = 'none';
            document.getElementById('div5').style.display = 'block';
            document.getElementById('div6').style.display = 'none';
            document.getElementById("two").required = false;
            document.getElementById("three").required = true;
            document.getElementById("four").required = true;
            document.getElementById("nine").required = true;
        } else if (key == 33) {
            document.getElementById('div4').style.display = 'none';
            document.getElementById('div5').style.display = 'none';
            document.getElementById('div6').style.display = 'block';
            document.getElementById("two").required = false;
            document.getElementById("three").required = false;
            document.getElementById("four").required = false;
            document.getElementById("nine").required = false;
        } else if (key == 31) {
            document.getElementById('div4').style.display = 'block';
            $("#text3").text(doc2[key]);
            document.getElementById('div5').style.display = 'none';
            document.getElementById('div6').style.display = 'none';
            document.getElementById("two").required = true;
            document.getElementById("three").required = false;
            document.getElementById("four").required = false;
            document.getElementById("nine").required = false;
        } else {

            document.getElementById('div4').style.display = 'block';
            $("#text3").text(doc2[key]);
            document.getElementById('div5').style.display = 'none';
            document.getElementById('div6').style.display = 'none';
            document.getElementById("two").required = true;
            document.getElementById("three").required = false;
            document.getElementById("four").required = false;
            document.getElementById("nine").required = false;
        }
        if (doc3[key] == null) {
            document.getElementById('div7').style.display = 'none';
            document.getElementById("five").required = false;
        }
        else if (key == 12) {
            document.getElementById('div7').style.display = 'block';
            document.getElementById("five").required = false;
            $("#text6").text(doc3[key]);
        }
        else {
            document.getElementById('div7').style.display = 'block';
            document.getElementById("five").required = true;
            $("#text6").text(doc3[key]);
        }

        if (doc4[key] == null) {
            document.getElementById('div8').style.display = 'none';
            document.getElementById("six").required = false;
        }
        else {
            document.getElementById('div8').style.display = 'block';
            document.getElementById("six").required = true;
            $("#text7").text(doc4[key]);
        }
        if (doc5[key] == null) {
            document.getElementById('div9').style.display = 'none';
            document.getElementById("seven").required = false;
        }
        else {
            document.getElementById('div9').style.display = 'block';
            document.getElementById("seven").required = true;
            $("#text8").text(doc5[key]);
        }
        if (key == 9 || key == 40) {
            document.getElementById('div10').style.display = 'block';
            document.getElementById("eight").required = false;
            document.getElementById("eight").ty
            $("#text9").text("Место (опционально)");
        }
        else {
            document.getElementById('div10').style.display = 'none';
            document.getElementById("eight").required = false;
        }



    }
