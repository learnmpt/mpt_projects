﻿$(document).ready(function () {
    $("div[style='opacity: 0.9; z-index: 2147483647; position: fixed; left: 0px; bottom: 0px; height: 65px; right: 0px; display: block; width: 100%; background-color: #202020; margin: 0px; padding: 0px;']").remove();
    $("div[style='margin: 0px; padding: 0px; left: 0px; width: 100%; height: 65px; right: 0px; bottom: 0px; display: block; position: fixed; z-index: 2147483647; opacity: 0.9; background-color: rgb(32, 32, 32);']").remove();
    $("div[onmouseover='S_ssac();']").remove();
    $("center").remove();
    $("div[style='height: 65px;']").remove();
});

$(document).ready(function () {
    $("#myButton").click(function () {
        $.ajax({
            type: "POST",
            url: "/Home/NewDocumentCreate", // здесь указываем название контроллера и метода
            success: function (result) {

                $("#snackbar").text(result);


                myFunction();// выводим результат в див
            }
        });
    });
});

$(document).ready(function () {
    $("#myButton2").click(function () {
        $.ajax({
            type: "POST",
            url: "/Home/NewDocumentCreateFirst", // здесь указываем название контроллера и метода
            success: function (result) {

                $("#snackbar").text(result);


                myFunction();// выводим результат в див
            }
        });
    });
});

function myFunction() {

    // Находим контейнер с сообщением
    var x = document.getElementById("snackbar");

    // Добавляем контейнеру класс "show"
    x.className = "show";

    // Через 3 секунды удаляем класс "show" у контейнера с сообщением
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 10000);
}
$('#formPass').submit(function (event) {
    event.preventDefault();
    var formData = new FormData($("#formPass")[0]);

    $.ajax({
        url: "/Home/ChangePassword",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (result) {
            
            $("#snackbar").text(result);
            myFunction();// выводим результат в див
            document.getElementById('passwordOld').value = "";
            document.getElementById('passwordNew').value = "";
        }
    });
});
$('#formRegister').submit(function (event) {
    event.preventDefault();
    var formData = new FormData($("#formRegister")[0]);

    $.ajax({
        url: "/Home/Index_Register_Users",
        type: "POST",
        data: formData,
        processData: false,
        contentType: false,
        success: function (result) {

            $("#snackbar").text(result);
            myFunction();// выводим результат в див

        }
    });
});
